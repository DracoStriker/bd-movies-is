﻿namespace GestorDeCinemas
{
    partial class EditProfile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.nameBox = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.ageBox = new System.Windows.Forms.TextBox();
            this.biBox = new System.Windows.Forms.TextBox();
            this.nifBox = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.femaleButton = new System.Windows.Forms.RadioButton();
            this.maleButton = new System.Windows.Forms.RadioButton();
            this.userSaveButton = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.savePassButton = new System.Windows.Forms.Button();
            this.confirmPassBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.oldPassBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.newPassBox = new System.Windows.Forms.TextBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.profileBox = new System.Windows.Forms.TextBox();
            this.profilePassBox = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // nameBox
            // 
            this.nameBox.Location = new System.Drawing.Point(61, 16);
            this.nameBox.Name = "nameBox";
            this.nameBox.Size = new System.Drawing.Size(148, 20);
            this.nameBox.TabIndex = 28;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 148);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(24, 13);
            this.label24.TabIndex = 46;
            this.label24.Text = "NIF";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 118);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(17, 13);
            this.label23.TabIndex = 45;
            this.label23.Text = "BI";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 84);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(42, 13);
            this.label22.TabIndex = 44;
            this.label22.Text = "Gender";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 50);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(26, 13);
            this.label21.TabIndex = 43;
            this.label21.Text = "Age";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 19);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(35, 13);
            this.label17.TabIndex = 42;
            this.label17.Text = "Name";
            // 
            // ageBox
            // 
            this.ageBox.Location = new System.Drawing.Point(61, 47);
            this.ageBox.Name = "ageBox";
            this.ageBox.Size = new System.Drawing.Size(148, 20);
            this.ageBox.TabIndex = 47;
            // 
            // biBox
            // 
            this.biBox.Location = new System.Drawing.Point(61, 115);
            this.biBox.Name = "biBox";
            this.biBox.Size = new System.Drawing.Size(148, 20);
            this.biBox.TabIndex = 48;
            // 
            // nifBox
            // 
            this.nifBox.Location = new System.Drawing.Point(61, 145);
            this.nifBox.Name = "nifBox";
            this.nifBox.Size = new System.Drawing.Size(148, 20);
            this.nifBox.TabIndex = 49;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(245, 275);
            this.tabControl1.TabIndex = 52;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.profileBox);
            this.tabPage1.Controls.Add(this.femaleButton);
            this.tabPage1.Controls.Add(this.maleButton);
            this.tabPage1.Controls.Add(this.userSaveButton);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.nameBox);
            this.tabPage1.Controls.Add(this.label21);
            this.tabPage1.Controls.Add(this.label22);
            this.tabPage1.Controls.Add(this.nifBox);
            this.tabPage1.Controls.Add(this.label23);
            this.tabPage1.Controls.Add(this.biBox);
            this.tabPage1.Controls.Add(this.label24);
            this.tabPage1.Controls.Add(this.ageBox);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(237, 249);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "User Profile";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // femaleButton
            // 
            this.femaleButton.AutoSize = true;
            this.femaleButton.Location = new System.Drawing.Point(115, 84);
            this.femaleButton.Name = "femaleButton";
            this.femaleButton.Size = new System.Drawing.Size(59, 17);
            this.femaleButton.TabIndex = 57;
            this.femaleButton.TabStop = true;
            this.femaleButton.Text = "Female";
            this.femaleButton.UseVisualStyleBackColor = true;
            // 
            // maleButton
            // 
            this.maleButton.AutoSize = true;
            this.maleButton.Location = new System.Drawing.Point(61, 84);
            this.maleButton.Name = "maleButton";
            this.maleButton.Size = new System.Drawing.Size(48, 17);
            this.maleButton.TabIndex = 56;
            this.maleButton.TabStop = true;
            this.maleButton.Text = "Male";
            this.maleButton.UseVisualStyleBackColor = true;
            // 
            // userSaveButton
            // 
            this.userSaveButton.Location = new System.Drawing.Point(134, 183);
            this.userSaveButton.Name = "userSaveButton";
            this.userSaveButton.Size = new System.Drawing.Size(75, 23);
            this.userSaveButton.TabIndex = 54;
            this.userSaveButton.Text = "Save";
            this.userSaveButton.UseVisualStyleBackColor = true;
            this.userSaveButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.profilePassBox);
            this.tabPage2.Controls.Add(this.savePassButton);
            this.tabPage2.Controls.Add(this.confirmPassBox);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.oldPassBox);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.newPassBox);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(237, 249);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Password";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // savePassButton
            // 
            this.savePassButton.Location = new System.Drawing.Point(134, 183);
            this.savePassButton.Name = "savePassButton";
            this.savePassButton.Size = new System.Drawing.Size(75, 23);
            this.savePassButton.TabIndex = 55;
            this.savePassButton.Text = "Save";
            this.savePassButton.UseVisualStyleBackColor = true;
            this.savePassButton.Click += new System.EventHandler(this.savePassButton_Click);
            // 
            // confirmPassBox
            // 
            this.confirmPassBox.Location = new System.Drawing.Point(103, 81);
            this.confirmPassBox.Name = "confirmPassBox";
            this.confirmPassBox.Size = new System.Drawing.Size(106, 20);
            this.confirmPassBox.TabIndex = 53;
            this.confirmPassBox.UseSystemPasswordChar = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 49;
            this.label1.Text = "Old Password";
            // 
            // oldPassBox
            // 
            this.oldPassBox.Location = new System.Drawing.Point(103, 16);
            this.oldPassBox.Name = "oldPassBox";
            this.oldPassBox.Size = new System.Drawing.Size(106, 20);
            this.oldPassBox.TabIndex = 48;
            this.oldPassBox.UseSystemPasswordChar = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 50;
            this.label2.Text = "New Password";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 51;
            this.label3.Text = "Confirm Password";
            // 
            // newPassBox
            // 
            this.newPassBox.Location = new System.Drawing.Point(103, 47);
            this.newPassBox.Name = "newPassBox";
            this.newPassBox.Size = new System.Drawing.Size(106, 20);
            this.newPassBox.TabIndex = 52;
            this.newPassBox.UseSystemPasswordChar = true;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // profileBox
            // 
            this.profileBox.Location = new System.Drawing.Point(61, 212);
            this.profileBox.Name = "profileBox";
            this.profileBox.ReadOnly = true;
            this.profileBox.Size = new System.Drawing.Size(148, 20);
            this.profileBox.TabIndex = 53;
            // 
            // profilePassBox
            // 
            this.profilePassBox.Location = new System.Drawing.Point(61, 212);
            this.profilePassBox.Name = "profilePassBox";
            this.profilePassBox.ReadOnly = true;
            this.profilePassBox.Size = new System.Drawing.Size(148, 20);
            this.profilePassBox.TabIndex = 56;
            // 
            // EditProfile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(265, 299);
            this.Controls.Add(this.tabControl1);
            this.Name = "EditProfile";
            this.Text = "Edit Profile";
            this.Load += new System.EventHandler(this.EditProfile_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox nameBox;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox ageBox;
        private System.Windows.Forms.TextBox biBox;
        private System.Windows.Forms.TextBox nifBox;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button userSaveButton;
        private System.Windows.Forms.RadioButton femaleButton;
        private System.Windows.Forms.RadioButton maleButton;
        private System.Windows.Forms.TextBox confirmPassBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox oldPassBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox newPassBox;
        private System.Windows.Forms.Button savePassButton;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.TextBox profileBox;
        private System.Windows.Forms.TextBox profilePassBox;
    }
}