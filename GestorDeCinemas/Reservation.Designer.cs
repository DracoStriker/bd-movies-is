﻿namespace GestorDeCinemas
{
    partial class Reservation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.idBox = new System.Windows.Forms.TextBox();
            this.dateBox = new System.Windows.Forms.TextBox();
            this.nTicketsBox = new System.Windows.Forms.TextBox();
            this.totalPriceBox = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.ticketsListGrid = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ticketsListGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel1.Controls.Add(this.idBox);
            this.panel1.Controls.Add(this.dateBox);
            this.panel1.Controls.Add(this.nTicketsBox);
            this.panel1.Controls.Add(this.totalPriceBox);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(161, 144);
            this.panel1.TabIndex = 59;
            // 
            // idBox
            // 
            this.idBox.Location = new System.Drawing.Point(73, 10);
            this.idBox.Name = "idBox";
            this.idBox.ReadOnly = true;
            this.idBox.Size = new System.Drawing.Size(71, 20);
            this.idBox.TabIndex = 56;
            // 
            // dateBox
            // 
            this.dateBox.Location = new System.Drawing.Point(73, 41);
            this.dateBox.Name = "dateBox";
            this.dateBox.ReadOnly = true;
            this.dateBox.Size = new System.Drawing.Size(71, 20);
            this.dateBox.TabIndex = 55;
            // 
            // nTicketsBox
            // 
            this.nTicketsBox.Location = new System.Drawing.Point(73, 75);
            this.nTicketsBox.Name = "nTicketsBox";
            this.nTicketsBox.ReadOnly = true;
            this.nTicketsBox.Size = new System.Drawing.Size(71, 20);
            this.nTicketsBox.TabIndex = 54;
            // 
            // totalPriceBox
            // 
            this.totalPriceBox.Location = new System.Drawing.Point(73, 109);
            this.totalPriceBox.Name = "totalPriceBox";
            this.totalPriceBox.ReadOnly = true;
            this.totalPriceBox.Size = new System.Drawing.Size(71, 20);
            this.totalPriceBox.TabIndex = 53;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(13, 13);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(18, 13);
            this.label17.TabIndex = 48;
            this.label17.Text = "ID";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(13, 44);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(30, 13);
            this.label21.TabIndex = 49;
            this.label21.Text = "Date";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(13, 78);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(57, 13);
            this.label22.TabIndex = 50;
            this.label22.Text = "Nº Tickets";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(13, 112);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(58, 13);
            this.label23.TabIndex = 51;
            this.label23.Text = "Total Price";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ticketsListGrid);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(179, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(423, 263);
            this.panel2.TabIndex = 61;
            // 
            // ticketsListGrid
            // 
            this.ticketsListGrid.AllowUserToAddRows = false;
            this.ticketsListGrid.AllowUserToDeleteRows = false;
            this.ticketsListGrid.AllowUserToResizeColumns = false;
            this.ticketsListGrid.AllowUserToResizeRows = false;
            this.ticketsListGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.ticketsListGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ticketsListGrid.Location = new System.Drawing.Point(16, 44);
            this.ticketsListGrid.MultiSelect = false;
            this.ticketsListGrid.Name = "ticketsListGrid";
            this.ticketsListGrid.ReadOnly = true;
            this.ticketsListGrid.RowHeadersVisible = false;
            this.ticketsListGrid.Size = new System.Drawing.Size(396, 198);
            this.ticketsListGrid.TabIndex = 62;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 61;
            this.label1.Text = "Tickets";
            // 
            // Reservation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(612, 286);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Reservation";
            this.Text = "Reservation";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ticketsListGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox idBox;
        private System.Windows.Forms.TextBox dateBox;
        private System.Windows.Forms.TextBox nTicketsBox;
        private System.Windows.Forms.TextBox totalPriceBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView ticketsListGrid;
    }
}