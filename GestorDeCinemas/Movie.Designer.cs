﻿namespace GestorDeCinemas
{
    partial class Movie
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.producerBox = new System.Windows.Forms.TextBox();
            this.nameBox = new System.Windows.Forms.TextBox();
            this.yearBox = new System.Windows.Forms.TextBox();
            this.durationBox = new System.Windows.Forms.TextBox();
            this.filmmakerBox = new System.Windows.Forms.TextBox();
            this.gendersGrid = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.summaryBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.actorsGrid = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.recommendButton = new System.Windows.Forms.Button();
            this.friendsGrid = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gendersGrid)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.actorsGrid)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.friendsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(13, 142);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(50, 13);
            this.label24.TabIndex = 52;
            this.label24.Text = "Producer";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(13, 112);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(54, 13);
            this.label23.TabIndex = 51;
            this.label23.Text = "Filmmaker";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(13, 78);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(47, 13);
            this.label22.TabIndex = 50;
            this.label22.Text = "Duration";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(13, 44);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(29, 13);
            this.label21.TabIndex = 49;
            this.label21.Text = "Year";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(13, 13);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(35, 13);
            this.label17.TabIndex = 48;
            this.label17.Text = "Name";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel1.Controls.Add(this.producerBox);
            this.panel1.Controls.Add(this.nameBox);
            this.panel1.Controls.Add(this.yearBox);
            this.panel1.Controls.Add(this.durationBox);
            this.panel1.Controls.Add(this.filmmakerBox);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.label24);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(151, 170);
            this.panel1.TabIndex = 58;
            // 
            // producerBox
            // 
            this.producerBox.Location = new System.Drawing.Point(73, 139);
            this.producerBox.Name = "producerBox";
            this.producerBox.ReadOnly = true;
            this.producerBox.Size = new System.Drawing.Size(71, 20);
            this.producerBox.TabIndex = 57;
            // 
            // nameBox
            // 
            this.nameBox.Location = new System.Drawing.Point(73, 10);
            this.nameBox.Name = "nameBox";
            this.nameBox.ReadOnly = true;
            this.nameBox.Size = new System.Drawing.Size(71, 20);
            this.nameBox.TabIndex = 56;
            // 
            // yearBox
            // 
            this.yearBox.Location = new System.Drawing.Point(73, 41);
            this.yearBox.Name = "yearBox";
            this.yearBox.ReadOnly = true;
            this.yearBox.Size = new System.Drawing.Size(71, 20);
            this.yearBox.TabIndex = 55;
            // 
            // durationBox
            // 
            this.durationBox.Location = new System.Drawing.Point(73, 75);
            this.durationBox.Name = "durationBox";
            this.durationBox.ReadOnly = true;
            this.durationBox.Size = new System.Drawing.Size(71, 20);
            this.durationBox.TabIndex = 54;
            // 
            // filmmakerBox
            // 
            this.filmmakerBox.Location = new System.Drawing.Point(73, 109);
            this.filmmakerBox.Name = "filmmakerBox";
            this.filmmakerBox.ReadOnly = true;
            this.filmmakerBox.Size = new System.Drawing.Size(71, 20);
            this.filmmakerBox.TabIndex = 53;
            // 
            // gendersGrid
            // 
            this.gendersGrid.AllowUserToAddRows = false;
            this.gendersGrid.AllowUserToDeleteRows = false;
            this.gendersGrid.AllowUserToResizeColumns = false;
            this.gendersGrid.AllowUserToResizeRows = false;
            this.gendersGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gendersGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gendersGrid.Location = new System.Drawing.Point(16, 37);
            this.gendersGrid.Name = "gendersGrid";
            this.gendersGrid.ReadOnly = true;
            this.gendersGrid.RowHeadersVisible = false;
            this.gendersGrid.Size = new System.Drawing.Size(122, 119);
            this.gendersGrid.TabIndex = 59;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.gendersGrid);
            this.panel2.Location = new System.Drawing.Point(169, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(151, 170);
            this.panel2.TabIndex = 60;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 61;
            this.label1.Text = "Genres";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.summaryBox);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Location = new System.Drawing.Point(12, 188);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(308, 189);
            this.panel3.TabIndex = 61;
            // 
            // summaryBox
            // 
            this.summaryBox.Location = new System.Drawing.Point(16, 42);
            this.summaryBox.Multiline = true;
            this.summaryBox.Name = "summaryBox";
            this.summaryBox.ReadOnly = true;
            this.summaryBox.Size = new System.Drawing.Size(279, 128);
            this.summaryBox.TabIndex = 63;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 62;
            this.label2.Text = "Summary";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.actorsGrid);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Location = new System.Drawing.Point(326, 12);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(173, 170);
            this.panel4.TabIndex = 62;
            // 
            // actorsGrid
            // 
            this.actorsGrid.AllowUserToAddRows = false;
            this.actorsGrid.AllowUserToDeleteRows = false;
            this.actorsGrid.AllowUserToResizeRows = false;
            this.actorsGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.actorsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.actorsGrid.Location = new System.Drawing.Point(15, 36);
            this.actorsGrid.MultiSelect = false;
            this.actorsGrid.Name = "actorsGrid";
            this.actorsGrid.ReadOnly = true;
            this.actorsGrid.RowHeadersVisible = false;
            this.actorsGrid.Size = new System.Drawing.Size(143, 119);
            this.actorsGrid.TabIndex = 63;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 62;
            this.label3.Text = "Actors";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.recommendButton);
            this.panel5.Controls.Add(this.friendsGrid);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Location = new System.Drawing.Point(326, 188);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(173, 189);
            this.panel5.TabIndex = 64;
            // 
            // recommendButton
            // 
            this.recommendButton.Location = new System.Drawing.Point(15, 147);
            this.recommendButton.Name = "recommendButton";
            this.recommendButton.Size = new System.Drawing.Size(143, 23);
            this.recommendButton.TabIndex = 64;
            this.recommendButton.Text = "Recommend Movie";
            this.recommendButton.UseVisualStyleBackColor = true;
            this.recommendButton.Click += new System.EventHandler(this.recommendButton_Click);
            // 
            // friendsGrid
            // 
            this.friendsGrid.AllowUserToAddRows = false;
            this.friendsGrid.AllowUserToDeleteRows = false;
            this.friendsGrid.AllowUserToResizeColumns = false;
            this.friendsGrid.AllowUserToResizeRows = false;
            this.friendsGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.friendsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.friendsGrid.Location = new System.Drawing.Point(15, 36);
            this.friendsGrid.Name = "friendsGrid";
            this.friendsGrid.ReadOnly = true;
            this.friendsGrid.RowHeadersVisible = false;
            this.friendsGrid.Size = new System.Drawing.Size(143, 105);
            this.friendsGrid.TabIndex = 63;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 13);
            this.label4.TabIndex = 62;
            this.label4.Text = "Your Friends";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // Movie
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(511, 388);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Movie";
            this.Text = "Movie";
            this.Load += new System.EventHandler(this.Movie_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gendersGrid)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.actorsGrid)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.friendsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView gendersGrid;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DataGridView actorsGrid;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox summaryBox;
        private System.Windows.Forms.TextBox filmmakerBox;
        private System.Windows.Forms.TextBox producerBox;
        private System.Windows.Forms.TextBox nameBox;
        private System.Windows.Forms.TextBox yearBox;
        private System.Windows.Forms.TextBox durationBox;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.DataGridView friendsGrid;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button recommendButton;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}