﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestorDeCinemas
{
    class MovieManager
    {
        private static readonly string database = "Data Source = tcp:193.136.175.33\\SQLSERVER2012,8293 ; Initial Catalog = p1g2 ; uid = p1g2 ; password = hugo&simon";

        public static HashSet<Ticket> ReservationList = new HashSet<Ticket>();

        public static string Database
        {
            get { return database; }
        }
    }

    class Ticket
    {
        private int preco;
	    private string date;
	    private string hora_da_sessao;
	    private int numero_da_sala;
	    private string localizacao_da_sala;
	    private char fila_do_lugar;
	    private int coluna_do_lugar;

        public Ticket(int preco, string date, string hora_da_sessao, int numero_da_sala, string localizacao_da_sala, char fila_do_lugar, int coluna_do_lugar)
        {
            this.preco = preco;
	        this.date = date;
	        this.hora_da_sessao = hora_da_sessao;
	        this.numero_da_sala = numero_da_sala;
	        this.localizacao_da_sala = localizacao_da_sala;
	        this.fila_do_lugar = fila_do_lugar;
            this.coluna_do_lugar = coluna_do_lugar;
        }
        public int getPreco() { return preco;}
        public int getSala() { return numero_da_sala; }
        public int getColuna() { return coluna_do_lugar; }
        public char getFila() { return fila_do_lugar;}
        public string getDate() { return date; }
        public string gethora() { return hora_da_sessao; }
        public string getCinema() { return localizacao_da_sala; }

       public static bool operator ==(Ticket a, Ticket b)
        {
            if (System.Object.ReferenceEquals(a, b))
            {
                return true;
            }
            if (((object)a == null) || ((object)b == null))
            {
                return false;
            }
            return (a.coluna_do_lugar == b.coluna_do_lugar) && 
                (a.fila_do_lugar == b.fila_do_lugar) &&
                (a.numero_da_sala == b.numero_da_sala) &&
                (a.localizacao_da_sala == b.localizacao_da_sala) &&
                (a.hora_da_sessao == b.hora_da_sessao) &&
                (a.date == b.date);
        }

        public static bool operator !=(Ticket a, Ticket b)
        {
            if (System.Object.ReferenceEquals(a, b))
            {
                return false;
            }
            if (((object)a == null) || ((object)b == null))
            {
                return true;
            }
            return (a.coluna_do_lugar != b.coluna_do_lugar) ||
                (a.fila_do_lugar != b.fila_do_lugar) ||
                (a.numero_da_sala != b.numero_da_sala) ||
                (a.localizacao_da_sala != b.localizacao_da_sala) ||
                (a.hora_da_sessao != b.hora_da_sessao) ||
                (a.date != b.date);
        }

        public void addToDataGridView(DataGridView grid)
        {
            grid.Rows.Add(new string[]{localizacao_da_sala,
                                    numero_da_sala.ToString(),
                                    fila_do_lugar.ToString(),
                                    coluna_do_lugar.ToString(),
                                    hora_da_sessao,
                                    date,
                                    preco.ToString()});
        }

        public override bool Equals(object obj)
        {
            Ticket b = (Ticket)obj;
            return (coluna_do_lugar == b.coluna_do_lugar) &&
                (fila_do_lugar == b.fila_do_lugar) &&
                (numero_da_sala == b.numero_da_sala) &&
                (localizacao_da_sala == b.localizacao_da_sala) &&
                (hora_da_sessao == b.hora_da_sessao) &&
                (date == b.date);
        }
    }
}
