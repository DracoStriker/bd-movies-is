﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace GestorDeCinemas
{
    public partial class Movie : Form
    {
        private SqlConnection con;
        private string username, movie, producer;

        public Movie(string username, string movie, string producer)
        {
            InitializeComponent();
            con = new SqlConnection(MovieManager.Database);
            this.username = username;
            this.movie = movie;
            this.producer = producer;
        }

        private void Movie_Load(object sender, EventArgs e)
        {
            con.Open();
            setMovieSpecs();
            setGendersGrid();
            setActorsGrid();
            setFriendsGrid();            
            con.Close();
        }

        private void setActorsGrid()
        {
            SqlCommand command;
            DataTable table;
            SqlDataAdapter adapter;
            table = new DataTable();
            command = new SqlCommand("actores_do_filme", con)
            {
                CommandType = CommandType.StoredProcedure
            };
            command.Parameters.Add(new SqlParameter("@nome_filme", movie));
            command.Parameters.Add(new SqlParameter("@nome_produtora", producer));
            adapter = new SqlDataAdapter(command);
            adapter.Fill(table);
            actorsGrid.DataSource = table;
            actorsGrid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void setFriendsGrid()
        {
            SqlCommand command;
            DataTable table;
            SqlDataAdapter adapter;
            table = new DataTable();
            command = new SqlCommand("amigos_membro", con)
            {
                CommandType = CommandType.StoredProcedure
            };
            command.Parameters.Add(new SqlParameter("@username", username));
            adapter = new SqlDataAdapter(command);
            adapter.Fill(table);
            friendsGrid.DataSource = table;
            friendsGrid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void setMovieSpecs()
        {
            SqlCommand command;
            SqlDataReader reader;
            command = new SqlCommand("select * from pf_filme;", con);
            reader = command.ExecuteReader();
            while (reader.Read())
            {
                if ((reader["nome"].ToString() == movie) && (reader["nome_da_produtora"].ToString() == producer))
                {
                    nameBox.Text = reader["nome"].ToString();
                    yearBox.Text = reader["ano"].ToString();
                    durationBox.Text = reader["duracao"].ToString();
                    filmmakerBox.Text = reader["id_do_realizador"].ToString();
                    producerBox.Text = reader["nome_da_produtora"].ToString();
                    summaryBox.Text = reader["descricao"].ToString();
                    break;
                }
            }
            reader.Close();
        }

        private void setGendersGrid()
        {
            SqlCommand command;
            DataTable table;
            SqlDataAdapter adapter;
            table = new DataTable();
            command = new SqlCommand("generos_do_filme", con)
            {
                CommandType = CommandType.StoredProcedure
            };
            command.Parameters.Add(new SqlParameter("@nome_filme", movie));
            command.Parameters.Add(new SqlParameter("@nome_produtora", producer));
            adapter = new SqlDataAdapter(command);
            adapter.Fill(table);
            gendersGrid.DataSource = table;
            gendersGrid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void recommendButton_Click(object sender, EventArgs e)
        {
            SqlCommand command;
            if (friendsGrid.RowCount > 0)
            {
                if (friendsGrid.SelectedRows.Count > 0 && friendsGrid.SelectedRows[0].Index < friendsGrid.RowCount)
                {
                    con.Open();
                    command = new SqlCommand("recomendar", con)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    command.Parameters.Add(new SqlParameter("@username", username));
                    command.Parameters.Add(new SqlParameter("@amigo", friendsGrid.SelectedRows[0].Cells[0].Value.ToString()));
                    command.Parameters.Add(new SqlParameter("@filme", movie));
                    command.Parameters.Add(new SqlParameter("@produtora", producer));
                    command.Parameters.Add(new SqlParameter("@return", SqlDbType.VarChar)
                    {
                        Direction = ParameterDirection.Output,
                        Size = 5
                    });
                    command.ExecuteNonQuery();
                    if (command.Parameters["@return"].Value.ToString() == "False")
                    {
                        errorProvider1.Clear();
                        errorProvider1.SetError(recommendButton, "Already recommended this " + movie + " to " + friendsGrid.SelectedRows[0].Cells[0].Value.ToString() + ".");
                    }
                    con.Close();
                }
                else
                {
                    errorProvider1.Clear();
                    errorProvider1.SetError(recommendButton, "Select one of your friends.");
                }
            }
            else
            {
                errorProvider1.Clear();
                errorProvider1.SetError(recommendButton, "You have no friends.");
            }
        }
    }
}
