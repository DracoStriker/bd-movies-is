﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace GestorDeCinemas
{
    public partial class Reservation : Form
    {
        private int id_reserva;
        private SqlConnection con;

        public Reservation(string id)
        {
            InitializeComponent();
            con = new SqlConnection(MovieManager.Database);
            this.id_reserva = Convert.ToInt32(id);
            con.Open();
            setContents();
            con.Close();
            setDataGridView2();
        }

        private void setDataGridView2()
        {
            ticketsListGrid.ColumnHeadersVisible = true;
            ticketsListGrid.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            ticketsListGrid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
        }

        private void setContents()
        {
            SqlCommand command;
            DataTable table;
            SqlDataAdapter adapter;
            table = new DataTable();
            command = new SqlCommand("get_reservation", con)
            {
                CommandType = CommandType.StoredProcedure
            };
            command.Parameters.Add(new SqlParameter("@id", SqlDbType.Int)
            {
                Value = id_reserva
            });
            command.Parameters.Add(new SqlParameter("@date", SqlDbType.Date)
            {
                Direction = ParameterDirection.Output
            });
            command.Parameters.Add(new SqlParameter("@nTickets", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            });
            command.Parameters.Add(new SqlParameter("@totalPrice", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            });
            adapter = new SqlDataAdapter(command);
            adapter.Fill(table);
            ticketsListGrid.DataSource = table;
            idBox.Text = id_reserva.ToString();
            dateBox.Text = command.Parameters["@date"].Value.ToString();
            nTicketsBox.Text = command.Parameters["@nTickets"].Value.ToString();
            totalPriceBox.Text = command.Parameters["@totalPrice"].Value.ToString();
        }
    }
}
