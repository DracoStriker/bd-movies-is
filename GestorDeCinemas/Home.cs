﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace GestorDeCinemas
{
    public partial class Home : Form
    {
        private Login login;
        private SqlConnection con;
        private string username;
        private int reservaID=0;
        
        public int ReservaID 
        { 
            get { return reservaID; }
            set { reservaID = value; }
        }
        
        public Home(Login login)
        {
            username = login.Username;
            InitializeComponent();
            con = new SqlConnection("Data Source = tcp:193.136.175.33\\SQLSERVER2012,8293 ; Initial Catalog = p1g2 ; uid = p1g2 ; password = hugo&simon");
            comboBox1.Items.AddRange(new String[] { "", "Accao", "Romance", "Comedia", "Aventura", "Terror", "Drama", "Misterio" });
            comboBox2.Items.AddRange(new String[] { "", "14:00:00", "17:00:00", "20:00:00", "23:00:00" });
            setDataGridView1();
            con.Open();
            fillProfile();
            setDataGridView6();
            setDataGridView2();
            setDataGridView5();
            setDataGridView7();
            setDataGridView9();
            setDataGridView8();
            setDataGridView10();
            setDataGridView3();
            con.Close();
            setDataGridView11();
            this.login = login;
        }

        private void setDataGridView1()
        {
            dataGridView1.ColumnCount = 2;
            dataGridView1.ColumnHeadersVisible = true;
            dataGridView1.Columns[0].Name = "Nome do Filme";
            dataGridView1.Columns[1].Name = "Produtora do Filme";
            dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void setDataGridView3()
        {
            DataTable table = new DataTable();
            SqlCommand command = new SqlCommand("select id, data from pf_reserva where user_do_membro = '" + username + "';", con);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(table);
            dataGridView3.DataSource = table;
            dataGridView3.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView3.ColumnHeadersVisible = true;
        }

        private void setDataGridView11()
        {
            ticketsGrid.ColumnCount = 7;
            ticketsGrid.Columns[0].Name = "Cinema";
            ticketsGrid.Columns[1].Name = "Sala";
            ticketsGrid.Columns[2].Name = "Fila";
            ticketsGrid.Columns[3].Name = "Coluna";
            ticketsGrid.Columns[4].Name = "Hora Inicio";
            ticketsGrid.Columns[5].Name = "Data";
            ticketsGrid.Columns[6].Name = "Preco";
            ticketsGrid.Rows.Clear();
            int precoTotal = 0, ntickets =0;
            foreach(Ticket t in MovieManager.ReservationList){
                t.addToDataGridView(ticketsGrid);
                precoTotal += t.getPreco();
                ntickets++;
            }

            textBox24.Text = ntickets.ToString();
            textBox21.Text = precoTotal.ToString();
        }

        private void setDataGridView10()
        {
            dataGridView10.ColumnCount = 1;
            dataGridView10.ColumnHeadersVisible = true;
            dataGridView10.Columns[0].Name = "Username";
            dataGridView10.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            dataGridView10.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            SqlCommand command = new SqlCommand("select user_do_membro2 from pf_amigo where user_do_membro1 = '"+username+"';", con);
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                dataGridView10.Rows.Add(reader["user_do_membro2"].ToString());
            }
            reader.Close();
            dataGridView10.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void setDataGridView8()
        {
            dataGridView8.ColumnCount = 2;
            dataGridView8.ColumnHeadersVisible = true;
            dataGridView8.Columns[0].Name = "Username";
            dataGridView8.Columns[1].Name = "Nome de User";
            dataGridView8.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            dataGridView8.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            SqlCommand command = new SqlCommand("select username, nome from pf_membro;", con);
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                dataGridView8.Rows.Add(reader["username"].ToString(), reader["nome"].ToString());
            }
            reader.Close();
            dataGridView8.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void setDataGridView9()
        {
            dataGridView9.ColumnCount = 5;
            dataGridView9.ColumnHeadersVisible = true;
            dataGridView9.Columns[0].Name = "Nome do Cinema";
            dataGridView9.Columns[1].Name = "Nome do Filme";
            dataGridView9.Columns[2].Name = "Hora de Inicio";
            dataGridView9.Columns[3].Name = "Data";
            dataGridView9.Columns[4].Name = "Numero Sala";
            dataGridView9.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            dataGridView9.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            SqlCommand command = new SqlCommand("select * from pf_sessao where convert(date,getdate())<= data and hora_de_inicio>convert(time,getdate());", con);
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
               dataGridView9.Rows.Add(reader["localizacao_da_sala"].ToString(), reader["nome_do_filme"].ToString(), reader["hora_de_inicio"].ToString(), reader["data"].ToString().Split(' ')[0], reader["numero_da_sala"].ToString());
            }
            reader.Close();
            dataGridView9.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void setDataGridView7()
        {
            DataTable table = new DataTable();
            SqlCommand command = new SqlCommand("select * from pf_cinema;", con);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(table);
            dataGridView7.DataSource = table;
            dataGridView7.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView7.ColumnHeadersVisible = true;
        }

        private void setDataGridView2()
        {
            DataTable table = new DataTable();
            SqlCommand command = new SqlCommand("select nome as Movie, nome_da_produtora as Producer from pf_filme;", con);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(table);
            dataGridView2.DataSource = table;
            dataGridView2.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView2.ColumnHeadersVisible = true;
        }

        private void setDataGridView6()
        {
            DataTable table = new DataTable();
            SqlCommand command = new SqlCommand("select nome_do_filme as Movie, nome_da_produtora as Producer, friend_user as Friend from pf_recomendacoes where username = '" + username + "';", con);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(table);
            dataGridView6.DataSource = table;
            dataGridView6.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView6.ColumnHeadersVisible = true;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            new EditProfile(username,this).Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            if (dataGridView6.RowCount < 1)
            {
                errorProvider1.Clear();
                errorProvider1.SetError(button5, "You have no recommendations.");
            }
            else
            {
                if (dataGridView6.SelectedRows.Count > 0 && dataGridView6.SelectedRows[0].Index < dataGridView6.RowCount)
                {
                    new Movie(username, dataGridView6.SelectedRows[0].Cells[0].Value.ToString(), dataGridView6.SelectedRows[0].Cells[1].Value.ToString()).Show();
                }
                else
                {
                    errorProvider1.Clear();
                    errorProvider1.SetError(button5, "Select a recommendation.");
                }
            }
        }

        public void fillInProfile(String nome, String idade, String sexo, String bi, String nif)
        {
            textBox16.Text = nome;
            textBox15.Text = idade;
            textBox7.Text = sexo;
            textBox19.Text = bi;
            textBox20.Text = nif;
        }

        private void fillProfile()
        {
            SqlCommand command = new SqlCommand("select nome,idade,sexo,bi,nif from pf_membro where username = '"+username+"';",con);
            SqlDataReader reader = command.ExecuteReader();
            reader.Read();
            textBox16.Text = reader["nome"].ToString();
            textBox15.Text = reader["idade"].ToString();
            textBox7.Text = reader["sexo"].ToString();
            textBox19.Text = reader["bi"].ToString();
            textBox20.Text = reader["nif"].ToString();
            reader.Close();
        }

        private void textBox11_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                errorProvider1.Clear();
                dataGridView2.DataSource = null;
                con.Open();
                setDataGridView2byName(textBox11.Text);
                con.Close();
            }
        }

        private void setDataGridView2byName(string p)
        {
            dataGridView2.Rows.Clear();
            DataTable table = new DataTable();
            SqlCommand command = new SqlCommand("select nome as Movie, nome_da_produtora as Producer from pf_filme where nome like '%" + p + "%';", con);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(table);
            dataGridView2.DataSource = table;
            dataGridView2.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView2.ColumnHeadersVisible = true;
        }

        private void textBox10_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                errorProvider1.Clear();
                dataGridView2.DataSource = null;
                con.Open();
                try
                {
                    setDataGridView2byYear(Convert.ToInt32(textBox10.Text));
                }
                catch (Exception ex)
                {
                    errorProvider1.Clear();
                    errorProvider1.SetError(textBox10, "Formato de ano invalido!");
                }
                con.Close();
            }
        }

        private void setDataGridView2byYear(int p)
        {
            System.DateTime m = new DateTime();
            if (p > 1900 || p <= m.Year)
            {
                dataGridView2.Rows.Clear();
                DataTable table = new DataTable();
                SqlCommand command = new SqlCommand("select nome as Movie, nome_da_produtora as Producer from pf_filme where ano = " + p + " ;", con); 
                SqlDataAdapter adapter = new SqlDataAdapter(command);                
                adapter.Fill(table);
                dataGridView2.DataSource = table;
                dataGridView2.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                dataGridView2.ColumnHeadersVisible = true;
            }
        }

        private void textBox9_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                errorProvider1.Clear();
                dataGridView2.DataSource = null;
                con.Open();
                setDataGridView2byFilmmaker(textBox9.Text);
                con.Close();
            }
        }

        private void setDataGridView2byFilmmaker(string p)
        {
            dataGridView2.Rows.Clear();
            DataTable table = new DataTable();
            SqlCommand command = new SqlCommand("select pf_filme.nome as Movie, pf_filme.nome_da_produtora as Producer from pf_filme,pf_realizador,pf_membro_da_staff where pf_membro_da_staff.nome like '%" + p + "%' and id= id_de_membro_da_staff and id_de_membro_da_staff = id_do_realizador;", con);
            SqlDataAdapter adapter = new SqlDataAdapter(command); 
            adapter.Fill(table);
            dataGridView2.DataSource = table;
            dataGridView2.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView2.ColumnHeadersVisible = true;
        }

        private void textBox8_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                errorProvider1.Clear();
                dataGridView2.DataSource = null;
                con.Open();
                setDataGridView2byProducer(textBox8.Text);
                con.Close();
            }
        }

        private void setDataGridView2byProducer(string p)
        {
            dataGridView2.Rows.Clear();
            DataTable table = new DataTable();
            SqlCommand command = new SqlCommand("select nome as Movie, nome_da_produtora as Producer from pf_filme where nome_da_produtora like '%" + p + "%';", con);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(table);
            dataGridView2.DataSource = table;
            dataGridView2.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView2.ColumnHeadersVisible = true;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            con.Open();
            dataGridView2.DataSource = null;
            setDataGridView2byGender(comboBox1.Text);
            con.Close();
        }

        private void setDataGridView2byGender(string p)
        {
            dataGridView2.Rows.Clear();
            
            if (p.CompareTo("") == 0)
            {
                setDataGridView2();
            }else{
                DataTable table = new DataTable();
                SqlCommand command = new SqlCommand("select nome_do_filme as Movie, nome_da_produtora as Producer from pf_genero_filme where genero = '" + p + "';", con);
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(table);
                dataGridView2.DataSource = table;
                dataGridView2.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                dataGridView2.ColumnHeadersVisible = true;
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                dataGridView2.DataSource = null;
                con.Open();
                setDataGridView2byActor(textBox1.Text);
                con.Close();
            }
        }

        private void setDataGridView2byActor(string p)
        {
            dataGridView2.Rows.Clear();
            DataTable table = new DataTable();
            SqlCommand command = new SqlCommand("select distinct pf_actua.nome_do_filme as Movie,pf_actua.produtora_do_filme as Producer from pf_actua, pf_actor, pf_membro_da_staff where pf_membro_da_staff.nome like '%" + p + "%' and id= id_de_membro_da_staff and id_de_membro_da_staff = id_do_actor;", con);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(table);
            dataGridView2.DataSource = table;
            dataGridView2.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView2.ColumnHeadersVisible = true;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView2.RowCount > 0)
            {
                if (!(dataGridView2.SelectedRows.Count > 0 && dataGridView2.SelectedRows[0].Index < dataGridView2.RowCount))
                {
                    errorProvider1.Clear();
                    errorProvider1.SetError(button4, "Select a movie to display.");
                }
                else
                {
                    new Movie(username, dataGridView2.SelectedRows[0].Cells[0].Value.ToString(), dataGridView2.SelectedRows[0].Cells[1].Value.ToString()).Show();
                }
            }
            else
            {
                errorProvider1.Clear();
                errorProvider1.SetError(button4, "There are no movies in the database.");
            }
        }

        private void textBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                con.Open();
                setDataGridView5byName(textBox5.Text);
                con.Close();
                radioButton1.Checked = false;
                radioButton2.Checked = false;
                textBox5.Text = "";
            }
        }

        private void setDataGridView5byName(string p)
        {
            SqlCommand command;
            dataGridView5.DataSource = null;
            DataTable table = new DataTable();
            SqlDataAdapter adapter = null;


            if (radioButton2.Checked)
            {
                 command = new SqlCommand("select distinct nome, id from pf_membro_da_staff, pf_actor where nome = '" + p + "' and id_de_membro_da_staff = id", con);
                 adapter = new SqlDataAdapter(command);
            }
            else if (radioButton1.Checked)
            {
                command = new SqlCommand("select distinct nome, id from pf_membro_da_staff, pf_realizador where nome = '" + p + "' and id_de_membro_da_staff = id", con);
                adapter = new SqlDataAdapter(command);
            }
            else
            {
                command = new SqlCommand("select distinct nome,id from pf_membro_da_staff, pf_actor, pf_realizador where nome = '" + p + "' and (pf_realizador.id_de_membro_da_staff = id or pf_actor.id_de_membro_da_staff = id);", con);
                adapter = new SqlDataAdapter(command);
            }

            adapter.Fill(table);
            dataGridView5.DataSource = table;
            dataGridView5.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView5.ColumnHeadersVisible = true;
        }

        private void setDataGridView5()
        {
            dataGridView5.DataSource = null;
            DataTable table = new DataTable();
            SqlCommand command = new SqlCommand("select nome, id from pf_membro_da_staff;", con);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(table);
            dataGridView5.DataSource = table;
            dataGridView5.ColumnHeadersVisible = true;
            dataGridView5.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dataGridView5.RowCount > 0)
            {
                if (!(dataGridView5.SelectedRows.Count > 0 && dataGridView5.SelectedRows[0].Index < dataGridView5.RowCount))
                {
                    errorProvider1.Clear();
                    errorProvider1.SetError(button1, "Select a cast member.");
                }
                else
                {
                    con.Open();
                    SqlCommand command;
                    DataTable table = new DataTable();
                    command = new SqlCommand("get_Cast_Info", con)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    int i = Convert.ToInt16(dataGridView5.SelectedRows[0].Cells[1].Value.ToString());
                    command.Parameters.Add(new SqlParameter("@id", i));
                    SqlDataReader reader = command.ExecuteReader();
                    reader.Read();
                    textBox25.Text = reader["nome"].ToString();
                    textBox27.Text = reader["idade"].ToString();
                    textBox26.Text = reader["sexo"].ToString();
                    reader.Close();
                    SqlCommand command1;
                    command1= new SqlCommand("get_Filmmaker",con)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    command1.Parameters.Add(new SqlParameter("@id",Convert.ToInt32(dataGridView5.SelectedRows[0].Cells[1].Value.ToString())));
                    SqlDataReader r1 = command1.ExecuteReader();
                    if (r1.Read())
                    {
                        textBox28.Text = "Realizador";
                        r1.Close();
                        setDataGridView1byCastMember("Realizador");
                    }
                    else
                    {
                        textBox28.Text = "Actor";
                        r1.Close();
                        setDataGridView1byCastMember("Actor");
                    }
                    con.Close();
                }
            }
            else
            {
                errorProvider1.Clear();
                errorProvider1.SetError(button1, "There are no members in the database.");
            }
            radioButton1.Checked = false;
            radioButton2.Checked = false;
            setDataGridView5();
        }

        private void Home_FormClosed(object sender, FormClosedEventArgs e)
        {
            login.Show();
            login.SetStartUpMessage("Login or Sign In.");
        }

        private void setDataGridView1byCastMember(string p)
        {
            dataGridView1.Rows.Clear();
            SqlCommand command1 = null;
            dataGridView1.Rows.Clear();
            int i = Convert.ToInt32(dataGridView5.SelectedRows[0].Cells[1].Value);
            if (p.CompareTo("Actor") == 0)
            {
                command1 = new SqlCommand("get_Actors", con)
                {
                    CommandType = CommandType.StoredProcedure
                };
                
                command1.Parameters.Add(new SqlParameter("@id", SqlDbType.Int) { Value = i });
                SqlDataReader r1 = command1.ExecuteReader();
                while (r1.Read())
                {
                    dataGridView1.Rows.Add(r1["Movie"].ToString(), r1["Producer"].ToString());
                }
                dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                dataGridView1.ColumnHeadersVisible = true;
            }
            else
            {
                command1 = new SqlCommand("get_Filmmakers", con)
                {
                    CommandType = CommandType.StoredProcedure
                };
                
                
                command1.Parameters.Add(new SqlParameter("@ii", SqlDbType.Int) { Value = i });
                Console.WriteLine(command1.Parameters[0]+": "+command1.Parameters[0].Value);
                SqlDataReader r1 = command1.ExecuteReader();
                while (r1.Read())
                {
                    dataGridView1.Rows.Add(r1["Movie"].ToString(), r1["Producer"].ToString());
                }
                dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                dataGridView1.ColumnHeadersVisible = true;
            }
        }

        private void textBox12_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                con.Open();
                setDataGridView7byName(textBox12.Text);
                con.Close();
            }
        }

        private void setDataGridView7byName(string p)
        {

            dataGridView7.DataSource = null;
            DataTable table = new DataTable();
            SqlCommand command = new SqlCommand("get_Cinemas_by_Name", con)
            {
                CommandType=CommandType.StoredProcedure
            };
            command.Parameters.AddWithValue("@name", p);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(table);
            dataGridView7.DataSource = table;
            dataGridView7.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView7.ColumnHeadersVisible = true;

         
        }

        private void dataGridView7_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            con.Open();
            dataGridView9.Rows.Clear();
            SqlCommand command = new SqlCommand("select hora_de_inicio, data, numero_da_sala, nome_do_filme, localizacao_da_sala from pf_sessao where convert(date,getdate())<= data and hora_de_inicio>convert(time,getdate()) and localizacao_da_sala= '" + dataGridView7.SelectedRows[0].Cells[0].Value.ToString() + "';", con);
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                dataGridView9.Rows.Add(reader["localizacao_da_sala"].ToString(), reader["nome_do_filme"].ToString(), reader["hora_de_inicio"].ToString(), reader["data"].ToString().Split(' ')[0], reader["numero_da_sala"].ToString());
            }
            reader.Close();
            con.Close();
        }

        private void textBox18_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                con.Open();
                setDataGridView9byProducer(textBox18.Text);
                con.Close();
            }
        }

        private void setDataGridView9byProducer(string p)
        {
            dataGridView9.Rows.Clear();
            SqlCommand command = new SqlCommand("select * from pf_sessao where convert(date,getdate())<= data and hora_de_inicio>convert(time,getdate()) and produtora_do_filme like '%" + p + "%';", con);
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                dataGridView9.Rows.Add(reader["localizacao_da_sala"].ToString(), reader["nome_do_filme"].ToString(), reader["hora_de_inicio"].ToString(), reader["data"].ToString().Split(' ')[0], reader["numero_da_sala"].ToString());
            }
            reader.Close();
        }

        private void textBox17_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                con.Open();
                setDataGridView9byName(textBox17.Text);
                con.Close();
            }
        }

        private void setDataGridView9byName(string p)
        {
            dataGridView9.Rows.Clear();
            SqlCommand command = new SqlCommand("select * from pf_sessao where convert(date,getdate())<= data and hora_de_inicio>convert(time,getdate()) and nome_do_filme like '%" + p + "%';", con);
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                dataGridView9.Rows.Add(reader["localizacao_da_sala"].ToString(), reader["nome_do_filme"].ToString(), reader["hora_de_inicio"].ToString(), reader["data"].ToString().Split(' ')[0], reader["numero_da_sala"].ToString());
            }
            reader.Close();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            con.Open();
            if (comboBox2.Text.CompareTo("") == 0)
            {
                setDataGridView9byName("");
            }
            else
            {
                setDataGridView9bytime(comboBox2.Text);
            }
            con.Close();
        }

        private void setDataGridView9bytime(string p)
        {
            dataGridView9.Rows.Clear();
            SqlCommand command = new SqlCommand("select * from pf_sessao where convert(date,getdate())<= data and hora_de_inicio>convert(time,getdate()) and datepart(hour,hora_de_inicio) =datepart(hour, '" + p + "');", con);
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                dataGridView9.Rows.Add(reader["localizacao_da_sala"].ToString(), reader["nome_do_filme"].ToString(), reader["hora_de_inicio"].ToString(), reader["data"].ToString().Split(' ')[0], reader["numero_da_sala"].ToString());
            }
            reader.Close();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (dataGridView9.RowCount < 1)
            {
                errorProvider1.Clear();
                errorProvider1.SetError(button7, "There are no sessions in the database.");
            }
            else
            {
                if (dataGridView9.SelectedRows.Count > 0 && dataGridView9.SelectedRows[0].Index < dataGridView9.RowCount)
                {

                    Console.WriteLine("data: " + dataGridView9.SelectedRows[0].Cells[3].Value.ToString());
                    Console.WriteLine("hora: " + dataGridView9.SelectedRows[0].Cells[2].Value.ToString());
                    Console.WriteLine("Sala: " + dataGridView9.SelectedRows[0].Cells[4].Value.ToString());
                    Console.WriteLine("Cinema: " + dataGridView9.SelectedRows[0].Cells[0].Value.ToString());
                    new Session(
                        dataGridView9.SelectedRows[0].Cells[2].Value.ToString(),
                        dataGridView9.SelectedRows[0].Cells[3].Value.ToString(),
                        dataGridView9.SelectedRows[0].Cells[4].Value.ToString(),
                        dataGridView9.SelectedRows[0].Cells[0].Value.ToString()
                    ).Show();
                }
                else
                {
                    errorProvider1.Clear();
                    errorProvider1.SetError(button7, "Select a session.");
                }
            }
        }

        private void textBox22_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                con.Open();
                setDataGridView8byName(textBox22.Text);
                con.Close();
            }
        }

        private void setDataGridView8byName(string p)
        {
            dataGridView8.Rows.Clear();
            SqlCommand command = new SqlCommand("search_member_by_Name", con)
            {
                CommandType = CommandType.StoredProcedure
            };
            command.Parameters.AddWithValue("@name",p);
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                dataGridView8.Rows.Add(reader["username"].ToString(), reader["nome"].ToString());
            }
            reader.Close();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            if (dataGridView8.RowCount > 0)
            {
                if (!(dataGridView8.SelectedRows.Count > 0 && dataGridView8.SelectedRows[0].Index < dataGridView8.RowCount))
                {
                    errorProvider1.Clear();
                    errorProvider1.SetError(button9, "Select a member.");
                }
                else
                {
                    con.Open();
                    addFriend(dataGridView8.SelectedRows[0].Cells[0].Value.ToString());
                    UpdateFriendsTable();
                    con.Close();
                }
            }
            else
            {
                errorProvider1.Clear();
                errorProvider1.SetError(button9, "No members found in the database.");
            }
        }

        private void addFriend(String friend)
        {
            if (friend.CompareTo(username) != 0)
            {
                SqlCommand command = new SqlCommand("add_Friend", con)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.AddWithValue("@user1", username);
                command.Parameters.AddWithValue("@user2", friend);
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    errorProvider1.Clear();
                    errorProvider1.SetError(button9, "That member is already your friend.");                    
                }
            }
        }

        private void UpdateFriendsTable()
        {
            dataGridView10.Rows.Clear();
            SqlCommand command = new SqlCommand("search_Friend", con)
            {
                CommandType = CommandType.StoredProcedure
            };
            command.Parameters.AddWithValue("@user1",username);
            command.Parameters.AddWithValue("@user2","");
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                dataGridView10.Rows.Add(reader["Friend"].ToString());
            }
            reader.Close();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            if (dataGridView10.RowCount < 1)
            {
                errorProvider1.Clear();
                errorProvider1.SetError(button10, "No friends found.");
            }
            else
            {
                if (dataGridView10.SelectedRows.Count > 0 && dataGridView10.SelectedRows[0].Index < dataGridView10.RowCount)
                {
                    con.Open();
                    removeFriend(dataGridView10.SelectedRows[0].Cells[0].Value.ToString());
                    SqlCommand command = new SqlCommand("remove_Friend", con)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    command.Parameters.AddWithValue("@user1",username);
                    command.Parameters.AddWithValue("@user2", username);
                    command.ExecuteNonQuery();
                    UpdateFriendsTable();
                    con.Close();
                }
                else
                {
                    errorProvider1.Clear();
                    errorProvider1.SetError(button10, "Select a friend.");
                }
            }
        }

        private void removeFriend(string p)
        {
            SqlCommand command = new SqlCommand("delete from pf_amigo where user_do_membro1 = '"+username+"' and user_do_membro2='"+p+"';", con);
            command.ExecuteNonQuery();
            command = new SqlCommand("delete from pf_amigo where user_do_membro2 = '" + username + "' and user_do_membro1='" + p + "';", con);
            command.ExecuteNonQuery();
        }   

        private void button14_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            deleteCart();
        }

        private void deleteCart()
        {
            if (MovieManager.ReservationList.Count != 0)
            {
                HashSet<Ticket> deletelist = new HashSet<Ticket>();
                foreach (Ticket t in MovieManager.ReservationList)
                    deletelist.Add(t);
                foreach (Ticket t in deletelist)
                    MovieManager.ReservationList.Remove(t);
                setDataGridView11();
            }
            else
            {
                errorProvider1.Clear();
                errorProvider1.SetError(button14, "No active Cart Found.");
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {
            if (ticketsGrid.RowCount < 1)
            {
                errorProvider1.Clear();
                errorProvider1.SetError(button13, "No Tickets found.");
            }
            else
            {
                if (ticketsGrid.SelectedRows.Count > 0 && ticketsGrid.SelectedRows[0].Index < ticketsGrid.RowCount)
                {
                    if (ticketsGrid.RowCount < 1)
                    {
                        errorProvider1.Clear();
                        errorProvider1.SetError(button13, "No Tickets found.");
                    }
                    else
                    {
                        Ticket a = new Ticket(0, 
                                    ticketsGrid.SelectedRows[0].Cells[5].Value.ToString(), 
                                    ticketsGrid.SelectedRows[0].Cells[4].Value.ToString(), 
                                    Convert.ToInt32(ticketsGrid.SelectedRows[0].Cells[1].Value.ToString()), 
                                    ticketsGrid.SelectedRows[0].Cells[0].Value.ToString(), 
                                    Convert.ToChar(ticketsGrid.SelectedRows[0].Cells[2].Value.ToString()), 
                                    Convert.ToInt32(ticketsGrid.SelectedRows[0].Cells[3].Value.ToString()));
                        Ticket b = null;
                        Console.WriteLine(
                            "preco: " + a.getPreco() + 
                            " data: " + a.getDate() + 
                            " hora: " + a.gethora() + 
                            " sala: " + a.getSala() + 
                            " cinema: " + a.getCinema() +
                            " fila: " + a.getFila() + 
                            " coluna: " + a.getColuna()
                            );
                        foreach (Ticket t in MovieManager.ReservationList)
                        {
                            Console.WriteLine(
                                "preco: " + t.getPreco() +
                                " data: " + t.getDate() +
                                " hora: " + t.gethora() + 
                                " sala: " + t.getSala() + 
                                " cinema: " + t.getCinema() +
                                " fila: " + t.getFila() + 
                                " coluna: " + t.getColuna()
                                );
                            if (t == a)
                            {
                                b = t;
                                Console.WriteLine("Estou aqui....");
                                break;
                            }
                        }
                        if (b != null)
                        {
                            MovieManager.ReservationList.Remove(b);
                        }
                        setDataGridView11();
                    }
                }
                else
                {
                    errorProvider1.Clear();
                    errorProvider1.SetError(button13, "Select a Ticket.");
                }
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            if(MovieManager.ReservationList.Count != 0)
            {
                con.Open();
                SqlCommand com = new SqlCommand("get_max_ReservaID", con)
                {
                    CommandType = CommandType.StoredProcedure
                };
                SqlDataReader r = com.ExecuteReader();
                r.Read();
                int max = Convert.ToInt16(r["maximo"].ToString()) + 1;
                r.Close();
                SqlCommand com1 = new SqlCommand("insert into pf_reserva values (convert(date,getdate())," + max + ", '" + username + "')", con);
                com1.ExecuteNonQuery();
                foreach (Ticket t in MovieManager.ReservationList)
                {
                    SqlCommand command = new SqlCommand("add_Bilhete", con)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    Console.WriteLine("data: "+t.getDate());
                    command.Parameters.AddWithValue("@price", t.getPreco());
                    command.Parameters.AddWithValue("@data", DateTime.Parse(t.getDate()));
                    command.Parameters.AddWithValue("@hora", t.gethora());
                    command.Parameters.AddWithValue("@sala", t.getSala());
                    command.Parameters.AddWithValue("@cinema", t.getCinema());
                    command.Parameters.AddWithValue("@fila", t.getFila());
                    command.Parameters.AddWithValue("@col", t.getColuna());
                    command.Parameters.AddWithValue("@id", max);
                    command.ExecuteNonQuery();
                }
                con.Close();
                deleteCart();
                setDataGridView3();
            }
            else
            {
                errorProvider1.SetError(button12,"No active Cart found!");
            }

        }

        private void button11_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            if (dataGridView6.RowCount > 0)
            {
                if (!(dataGridView6.SelectedRows.Count > 0 && dataGridView6.SelectedRows[0].Index < dataGridView6.RowCount))
                {
                    errorProvider1.Clear();
                    errorProvider1.SetError(button11, "Select a recommended movie.");
                }
                else
                {
                    con.Open();
                    SqlCommand com = new SqlCommand("remove_Recommendation", con)
                    {
                        CommandType= CommandType.StoredProcedure
                    };
                    com.Parameters.AddWithValue("@user",username);
                    com.Parameters.AddWithValue("@movie", dataGridView6.SelectedRows[0].Cells[0].Value.ToString());
                    com.Parameters.AddWithValue("@prod", dataGridView6.SelectedRows[0].Cells[1].Value.ToString());
                    com.ExecuteNonQuery();
                    con.Close();
                }
                setDataGridView6();
            }
            else
            {
                errorProvider1.Clear();
                errorProvider1.SetError(button11, "No recommendations found.");
            } 
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (dataGridView3.RowCount > 0)
            {
                if (!(dataGridView3.SelectedRows.Count > 0 && dataGridView3.SelectedRows[0].Index < dataGridView3.RowCount))
                {
                    errorProvider1.Clear();
                    errorProvider1.SetError(button11, "Select a reservation.");
                }
                else
                {
                    new Reservation(dataGridView3.SelectedRows[0].Cells[0].Value.ToString()).Show();
                }
            }
            else
            {
                errorProvider1.Clear();
                errorProvider1.SetError(button6, "You have no reservations.");
            } 
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                con.Open();
                setDataGridView6byName(textBox2.Text);
                con.Close();
            }
        }

        private void setDataGridView6byName(string p)
        {
            dataGridView6.DataSource = null;
            DataTable table = new DataTable();
            SqlCommand command = new SqlCommand("select nome_do_filme as Movie, nome_da_produtora as Producer, friend_user as Friend from pf_recomendacoes where username = '" + username + "' and ( friend_user like '%" + p + "%' or nome_do_filme like '%" + p + "%' or nome_da_produtora like '%" + p + "%') ;", con);
            SqlDataAdapter a = new SqlDataAdapter(command);
            a.Fill(table);
            dataGridView6.DataSource = table;
            dataGridView6.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView6.ColumnHeadersVisible = true;
        }

        private void textBox23_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                con.Open();
                dataGridView10.Rows.Clear();
                setDataGridView10byName(textBox23.Text);
                con.Close();
            }
        }

        private void setDataGridView10byName(string p)
        {
            SqlCommand command = new SqlCommand("search_Friend", con)
            {
                CommandType = CommandType.StoredProcedure
            };
            command.Parameters.AddWithValue("@user1",username);
            command.Parameters.AddWithValue("@user2", p);
            SqlDataReader r = command.ExecuteReader();
            while (r.Read())
            {
                dataGridView10.Rows.Add(r["Friend"].ToString());
            }
        }

        private void tabControl1_TabIndexChanged(object sender, EventArgs e)
        {
            Console.WriteLine("Vim praqui!!!!!!!");
            setDataGridView11();
        }

        private void tabControl1_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Vim praqui....!!!!!!!");
            setDataGridView11();
        }    
    }
}