﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace GestorDeCinemas
{
    public partial class Login : Form
    {
        private SqlConnection con;
        private string username;

        public string Username { get { return username; } }

        public Login()
        {
            InitializeComponent();
            con = new SqlConnection(MovieManager.Database);
            messageBox.Text = "Login or Sign In.";
        }

        public void SetStartUpMessage(string s)
        {
            messageBox.Text = s;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlCommand command;
            if (usernameBox.Text.CompareTo("") == 0)
            {
                errorProvider1.Clear();
                errorProvider1.SetError(usernameBox, "Insert a username.");
                return;
            }
            if (passwordBox.Text.CompareTo("") == 0)
            {
                errorProvider1.Clear();
                errorProvider1.SetError(passwordBox, "Insert a password.");
                return;
            }
            con.Open();
            command = new SqlCommand("validate_registry", con)
            {
                CommandType = CommandType.StoredProcedure
            };
            command.Parameters.Add(new SqlParameter("@username", usernameBox.Text));
            command.Parameters.Add(new SqlParameter("@pwd", passwordBox.Text));
            command.Parameters.Add(new SqlParameter("@return", SqlDbType.VarChar)
            { 
                Direction = ParameterDirection.Output,
                Size = 5
            });
            errorProvider1.Clear();
            command.ExecuteNonQuery();
            if (command.Parameters["@return"].Value.ToString() == "False")
            {
                errorProvider1.SetError(usernameBox, "Username already registred.");
                messageBox.Text = "Login or Sign In.";
            }
            else
            {
                messageBox.Text = "Register Completed!";
            }
            con.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlCommand command;
            if (usernameBox.Text.CompareTo("") == 0)
            {
                errorProvider1.Clear();
                errorProvider1.SetError(usernameBox, "Insert a username.");
                return;
            }
            if (passwordBox.Text.CompareTo("") == 0)
            {
                errorProvider1.Clear();
                errorProvider1.SetError(passwordBox, "Insert a password.");
                return;
            }
            con.Open();
            command = new SqlCommand("validate_login", con)
            {
                CommandType = CommandType.StoredProcedure
            };
            command.Parameters.Add(new SqlParameter("@username", usernameBox.Text));
            command.Parameters.Add(new SqlParameter("@pwd", passwordBox.Text));
            command.Parameters.Add(new SqlParameter("@return", SqlDbType.VarChar)
            {
                Direction = ParameterDirection.Output,
                Size = 5
            });
            errorProvider1.Clear();
            command.ExecuteNonQuery();
            if (command.Parameters["@return"].Value.ToString() == "False")
            {
                errorProvider1.SetError(usernameBox, "Wrong username or password.");
                messageBox.Text = "Login or Sign In.";
            }
            else
            {
                username = usernameBox.Text;
                new Home(this).Show();
                errorProvider1.Clear();
                messageBox.Text = "Login Completed!";
                Hide();
            }
            con.Close();
        }
    }
}
