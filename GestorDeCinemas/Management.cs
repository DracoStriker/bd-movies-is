﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace GestorDeCinemas
{
    public partial class Management : Form
    {
        private SqlConnection con;
        public Management()
        {
            InitializeComponent();
            con = new SqlConnection("Data Source = tcp:193.136.175.33\\SQLSERVER2012,8293 ; Initial Catalog = p1g2 ; uid = p1g2 ; password = hugo&simon");
            con.Open();
            setDataGridView6();
            setDataGridView2();
            setDataGridView5();
            setDataGridView3();
            setDataGridView4();
            setDataGridView9();
            con.Close();
            setcombobox3();
            setcombobox4();
            /*Define Genders*/
            comboBox1.Items.AddRange(new String[] {"", "Accao", "Romance","Comedia","Terror","Drama","Misterio","Aventura"});

            /*define time for film's duration*/
            comboBox7.Items.AddRange(new string[] {"00","01","02"});
            for (int i = 0; i < 60; i++)
            {
                if(i<10)
                    comboBox8.Items.Add("0"+i.ToString());
                else
                    comboBox8.Items.Add(i.ToString());
            }
            DateTime d = DateTime.Now;
            for (int i = 1950; i <= d.Year; i++)
            {
                comboBox6.Items.Add(i.ToString());
            }
            for (int i = d.Year; i <= d.Year + 2; i++)
            {
                comboBox11.Items.Add(i.ToString());
            }
            for (int i = 1; i <= 12; i++)
            {
                if(i<10)
                    comboBox9.Items.Add("0"+i);
                else
                    comboBox9.Items.Add(i.ToString());
            }
            for (int i = 1; i < 32; i++)
            {
                if (i < 10)
                {
                    comboBox10.Items.Add("0" + i);
                    comboBox12.Items.Add(i.ToString());
                }
                else
                    comboBox10.Items.Add(i.ToString());
            }
            comboBox5.Items.AddRange(new String[]{"14:00:00","17:00:00","20:00:00","23:00:00"});
        }

        private void setDataGridView9()
        {
            dataGridView9.DataSource = null;
            DataTable table = new DataTable();
            SqlCommand command = new SqlCommand("select nome, id from pf_membro_da_staff;", con);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(table);
            dataGridView9.DataSource = table;
            dataGridView9.ColumnHeadersVisible = true;
            dataGridView9.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void setDataGridView4()
        {
            dataGridView4.Rows.Clear();
            dataGridView4.ColumnCount = 5;
            dataGridView4.ColumnHeadersVisible = true;
            dataGridView4.Columns[0].Name = "Nome do Cinema";
            dataGridView4.Columns[1].Name = "Nome do Filme";
            dataGridView4.Columns[2].Name = "Hora de Inicio";
            dataGridView4.Columns[3].Name = "Data";
            dataGridView4.Columns[4].Name = "Numero Sala";
            dataGridView4.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            dataGridView4.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            SqlCommand command = new SqlCommand("select * from pf_sessao;", con);
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                dataGridView4.Rows.Add(reader["localizacao_da_sala"].ToString(), reader["nome_do_filme"].ToString(), reader["hora_de_inicio"].ToString(), reader["data"].ToString().Split(' ')[0], reader["numero_da_sala"].ToString());
            }
            reader.Close();
            dataGridView4.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void setDataGridView3()
        {
            dataGridView3.DataSource = null;
            SqlCommand com = new SqlCommand("get_Cinemas", con)
            {
                CommandType = CommandType.StoredProcedure
            };
            SqlDataAdapter ad = new SqlDataAdapter(com);
            DataTable table = new DataTable();
            ad.Fill(table);
            dataGridView3.DataSource = table;
        }

        private void setDataGridView5()
        {
            dataGridView5.DataSource = null;
            SqlCommand com = new SqlCommand("get_Movies", con) { 
                CommandType = CommandType.StoredProcedure
            };
            SqlDataAdapter ad = new SqlDataAdapter(com);
            DataTable table = new DataTable();
            ad.Fill(table);
            dataGridView5.DataSource = table;
        }

        private void setcombobox4()
        {
            con.Open();
            SqlCommand command = new SqlCommand("select nome, id from pf_membro_da_staff, pf_realizador where id = id_de_membro_da_staff;", con);
            SqlDataReader r = command.ExecuteReader();
            comboBox4.Items.Clear();
            comboBox4.Items.Add("");
            while (r.Read())
            {
                comboBox4.Items.Add(r["nome"].ToString() + " - " + r["id"].ToString());
            }
            r.Close();
            con.Close();
        }

        private void setcombobox3()
        {
            con.Open();
            SqlCommand command = new SqlCommand("select nome, id from pf_membro_da_staff, pf_actor where id = id_de_membro_da_staff;",con);
            SqlDataReader r = command.ExecuteReader();
            comboBox3.Items.Clear();
            comboBox3.Items.Add("");
            while (r.Read())
            {
                comboBox3.Items.Add(r["nome"].ToString() + " - "+ r["id"].ToString());
            }
            r.Close();
            con.Close();
        }

        private void setDataGridView2()
        {
            /*movies*/
            dataGridView2.DataSource = null;
            SqlCommand command;
            DataTable table;
            SqlDataAdapter adapter;
            table = new DataTable();
            command = new SqlCommand("get_Movies", con)
            {
                CommandType = CommandType.StoredProcedure
            };
            adapter = new SqlDataAdapter(command);
            adapter.Fill(table);
            dataGridView2.DataSource = table;
            dataGridView2.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void setDataGridView6()
        {
            dataGridView6.DataSource = null;
            /*cinemas*/
            SqlCommand command;
            DataTable table;
            SqlDataAdapter adapter;
            table = new DataTable();
            command = new SqlCommand("get_Cinemas", con)
            {
                CommandType = CommandType.StoredProcedure
            };
            adapter = new SqlDataAdapter(command);
            adapter.Fill(table);
            dataGridView6.DataSource = table;
            dataGridView6.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void dataGridView6_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            con.Open();
            setDataGridView1byCinema(dataGridView6.SelectedRows[0].Cells[0].Value.ToString());
            con.Close();
        }

        private void setDataGridView1byCinema(string cinema)
        {
            dataGridView1.DataSource = null;
            /*get rooms of cinema selected*/
            SqlCommand command;
            DataTable table;
            SqlDataAdapter adapter;
            table = new DataTable();
            command = new SqlCommand("get_Rooms_from_Cinema", con)
            {
                CommandType = CommandType.StoredProcedure
            };
            command.Parameters.Add(new SqlParameter("@nome_Cinema", cinema));
            adapter = new SqlDataAdapter(command);
            adapter.Fill(table);
            dataGridView1.DataSource = table;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            string cinema = textBox7.Text;
            if (cinema.CompareTo("")==0 || cinema.Length > 32)
            {
                errorProvider1.Clear();
                errorProvider1.SetError(button1, "Nome Invalido!");
            }
            else
            {
                /*add cinema*/
                con.Open();
                SqlCommand command; 
                DataTable table;
                table = new DataTable();
                command = new SqlCommand("add_Cinema", con)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.Add(new SqlParameter("@name",cinema));
                try
                {
                    command.ExecuteNonQuery();
                    setDataGridView6();
                }
                catch (Exception exxx)
                {
                    errorProvider1.SetError(button1, "Cinema already exists!");
                }
                finally
                {
                    con.Close();
                }

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView6.SelectedRows.Count > 0 && dataGridView6.SelectedRows[0].Index < dataGridView6.RowCount)
            {
                con.Open();
                string cinema = dataGridView6.SelectedRows[0].Cells[0].Value.ToString();
                /*add a room to cinema*/
                SqlCommand command;
                //DataTable table = new DataTable();
                command = new SqlCommand("add_New_Room", con)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.Add(new SqlParameter("@cinema", cinema));
                command.ExecuteNonQuery();
                con.Close();
                setDataGridView1byCinema(cinema);
            }
            else
            {
                errorProvider1.Clear();
                errorProvider1.SetError(button2,"Select a cinema!");
            }
        }

        private void textBox6_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                con.Open();
                setDataGridView6byName(textBox6.Text);
                con.Close();
            }
        }

        private void setDataGridView6byName(string cinema)
        {
            dataGridView6.DataSource = null;
            /*search cinemas by name p*/
            SqlCommand command;
            DataTable table;
            SqlDataAdapter adapter;
            table = new DataTable();
            command = new SqlCommand("get_Cinemas_by_Name", con)
            {
                CommandType = CommandType.StoredProcedure
            };
            command.Parameters.Add(new SqlParameter("@name", cinema));
            adapter = new SqlDataAdapter(command);
            adapter.Fill(table);
            dataGridView6.DataSource = table;
            dataGridView6.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void textBox8_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                con.Open();
                setDataGridView2byName(textBox8.Text);
                con.Close();
            }
        }

        private void setDataGridView2byName(string movie)
        {
            dataGridView2.DataSource = null;
            /*search movies by name movie*/
            SqlCommand command;
            DataTable table;
            SqlDataAdapter adapter;
            table = new DataTable();
            command = new SqlCommand("get_Movies_by_Name", con)
            {
                CommandType = CommandType.StoredProcedure
            };
            command.Parameters.Add(new SqlParameter("@name", movie));
            adapter = new SqlDataAdapter(command);
            adapter.Fill(table);
            dataGridView2.DataSource = table;
            dataGridView2.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            if (dataGridView2.SelectedRows.Count > 0 && dataGridView2.SelectedRows[0].Index < dataGridView2.RowCount)
            {
                string movie = dataGridView2.SelectedRows[0].Cells[0].Value.ToString();
                string producer = dataGridView2.SelectedRows[0].Cells[1].Value.ToString();
                if (comboBox1.Text.CompareTo("") == 0)
                {
                    errorProvider1.SetError(button5, "Select a Gender!");
                }
                else
                {
                    con.Open();
                    string gender = comboBox1.Text;
                    SqlCommand command;
                    command = new SqlCommand("add_Gender", con)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    command.Parameters.Add(new SqlParameter("@movie", movie));
                    command.Parameters.Add(new SqlParameter("@producer", producer));
                    command.Parameters.Add(new SqlParameter("@gen", gender));
                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        errorProvider1.SetError(button5, "Error on adding a gender to the movie");
                    }
                    finally
                    {
                        con.Close();
                    }
                    
                }
            }
            else
            {
                errorProvider1.SetError(button5, "Seelct a movie");
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            
            string gender = comboBox2.Text;
            string movie = dataGridView2.SelectedRows[0].Cells[0].Value.ToString();
            string producer = dataGridView2.SelectedRows[0].Cells[1].Value.ToString();
            if (gender.CompareTo("") == 0)
            {
                errorProvider1.SetError(button6, "Select a Gender!");
            }
            else
            {
                con.Open();
                SqlCommand command;
                command = new SqlCommand("remove_Gender", con)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.Add(new SqlParameter("@movie", movie));
                command.Parameters.Add(new SqlParameter("@prod", producer));
                command.Parameters.Add(new SqlParameter("@gen", gender));
                try
                {
                    command.ExecuteNonQuery();
                    comboBox2.Items.Remove(gender);
                }
                catch (Exception ex)
                {
                    errorProvider1.SetError(button6, "Gender selected is invalid!");
                }
                finally
                {
                    con.Close();
                }
            }
            comboBox2.Text = "";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            if (comboBox6.Text.CompareTo("") == 0 || comboBox4.Text.CompareTo("")==0 ||
                comboBox7.Text.CompareTo("") == 0 || comboBox8.Text.CompareTo("") == 0 || textBox13.Text.CompareTo("")==0)
            {
                errorProvider1.SetError(button4, "Invalid data!");
            }
            else
            {
                if (dataGridView2.SelectedRows.Count < 1)
                {
                    errorProvider1.SetError(button11, "Select a Movie!");
                }
                else
                {
                    con.Open();
                    
                    try
                    {
                        string duracao = comboBox7.Text + ":" + comboBox8.Text + ":00";
                        int idRealizador = Convert.ToInt16(comboBox4.Text.Split(' ')[2]);
                        string desc = textBox13.Text;
                        int ano = Convert.ToInt16(comboBox6.Text);
                        string movie = dataGridView2.SelectedRows[0].Cells[0].Value.ToString();
                        string prod = dataGridView2.SelectedRows[0].Cells[1].Value.ToString();
                        SqlCommand command = new SqlCommand("update_Movie", con)
                        {
                            CommandType = CommandType.StoredProcedure
                        };
                        command.Parameters.AddWithValue("@movie", movie);
                        command.Parameters.AddWithValue("@time", duracao);
                        command.Parameters.AddWithValue("@filmmaker", idRealizador);
                        command.Parameters.AddWithValue("@description", desc);
                        command.Parameters.AddWithValue("@year", ano);
                        command.Parameters.AddWithValue("@prod", prod);
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        errorProvider1.SetError(button4,"Erro a actualizar informacao!");
                    }
                    finally
                    {
                        con.Close();
                    }
                }
                comboBox6.Text="";
                comboBox4.Text="";
                comboBox7.Text="";
                comboBox8.Text = "";
                textBox13.Text = "";
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            if (textBox5.Text.CompareTo("") == 0 || textBox4.Text.CompareTo("") == 0)
            {
                errorProvider1.SetError(button3,"Fill the Name and Producer's fields.");
            }
            else
            {
                con.Open();
                string movie = textBox5.Text;
                string producer = textBox4.Text;
                SqlCommand command;
                command = new SqlCommand("add_Movie", con)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.Add(new SqlParameter("@movie", movie));
                command.Parameters.Add(new SqlParameter("@prod", producer));                try
                {
                    command.ExecuteNonQuery();
                    setDataGridView2();
                }
                catch (Exception ex)
                {
                    errorProvider1.SetError(button3, "Film already exists!");
                }
                finally
                {
                    con.Close();
                }
            }
            textBox5.Text = "";
            textBox4.Text = "";

        }

        private void dataGridView2_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            comboBox2.Items.Clear();
            if (dataGridView2.SelectedRows.Count > 0 && dataGridView2.SelectedRows[0].Index < dataGridView2.RowCount)
            {
                string movie = dataGridView2.SelectedRows[0].Cells[0].Value.ToString();
                string producer = dataGridView2.SelectedRows[0].Cells[1].Value.ToString();
                con.Open();

                SqlCommand command;
                SqlDataReader reader;
                command = new SqlCommand("get_Genders", con)
                {
                    CommandType = CommandType.StoredProcedure
                };
                command.Parameters.Add(new SqlParameter("@movie", movie));
                command.Parameters.Add(new SqlParameter("@prod", producer));
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    comboBox2.Items.Add(reader["genero"].ToString());
                }
                con.Close();
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            if (dataGridView2.SelectedRows.Count < 1)
            {
                errorProvider1.SetError(button11, "Select a Movie!");
            }
            else
            {
                if (textBox2.Text.CompareTo("") == 0 || comboBox3.Text.CompareTo("") == 0)
                {
                    errorProvider1.SetError(button11, "Choose an actor and his role!");
                }
                else
                {
                    con.Open();
                    string role = textBox2.Text;
                    int idActor = Convert.ToInt16(comboBox3.Text.Split(' ')[2]);
                    string movie = dataGridView2.SelectedRows[0].Cells[0].Value.ToString();
                    string prod = dataGridView2.SelectedRows[0].Cells[1].Value.ToString();

                    SqlCommand command = new SqlCommand("add_Actor_To_Movie", con)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    command.Parameters.AddWithValue("@actor",idActor);
                    command.Parameters.AddWithValue("@role", role);
                    command.Parameters.AddWithValue("@movie", movie);
                    command.Parameters.AddWithValue("@prod", prod);
                   
                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        errorProvider1.SetError(button11,"Actor selected is already added to the movie!");
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

        }

        private void dataGridView3_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView3.SelectedRows.Count >= 1 && dataGridView3.SelectedRows[0].Index < dataGridView3.RowCount)
            {
                setDataGridView7byCinema(dataGridView3.SelectedRows[0].Cells[0].Value.ToString());
                textBox19.Text = dataGridView3.SelectedRows[0].Cells[0].Value.ToString();
            }            
        }

        private void setDataGridView7byCinema(string cinema)
        {
            dataGridView7.DataSource = null;
            /*get rooms of cinema selected*/
            SqlCommand command;
            DataTable table;
            SqlDataAdapter adapter;
            table = new DataTable();
            command = new SqlCommand("get_Rooms_from_Cinema", con)
            {
                CommandType = CommandType.StoredProcedure
            };
            command.Parameters.Add(new SqlParameter("@nome_Cinema", cinema));
            adapter = new SqlDataAdapter(command);
            adapter.Fill(table);
            dataGridView7.DataSource = table;
            dataGridView7.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void dataGridView5_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView5.SelectedRows.Count >= 1 && dataGridView5.SelectedRows[0].Index < dataGridView5.RowCount)
            {
                textBox17.Text= dataGridView5.SelectedRows[0].Cells[0].Value.ToString();
            }
        }

        private void dataGridView7_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView7.SelectedRows.Count >= 1 && dataGridView7.SelectedRows[0].Index < dataGridView7.RowCount)
            {
                textBox1.Text = dataGridView7.SelectedRows[0].Cells[0].Value.ToString();
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            try
            {
                con.Open();
                string movie = textBox17.Text;
                string producer = dataGridView5.SelectedRows[0].Cells[1].Value.ToString();
                string hora = comboBox5.Text;
                string data = comboBox11.Text+"-"+comboBox9.Text+"-"+comboBox10.Text;
                int sala = Convert.ToInt16(textBox1.Text);
                string cinema = textBox19.Text;
                int preco = Convert.ToInt16(comboBox12.Text);
                SqlCommand com = new SqlCommand("add_Session", con)
                {
                    CommandType = CommandType.StoredProcedure
                };
                com.Parameters.AddWithValue("@movie",movie);
                com.Parameters.AddWithValue("@prod", producer);
                com.Parameters.AddWithValue("@hour", hora);
                com.Parameters.AddWithValue("@date", data);
                com.Parameters.AddWithValue("@sala", sala);
                com.Parameters.AddWithValue("@cinema", cinema);
                com.Parameters.AddWithValue("@price", preco);
                com.ExecuteNonQuery();
                setDataGridView4();
            }
            catch (Exception ex)
            {
                errorProvider1.SetError(button7,"Invalid information.");
            }
            finally
            {
                con.Close();
            }
            textBox17.Text = "";
            textBox19.Text = "";
            textBox1.Text = "";
            comboBox5.Text = "";
            comboBox9.Text = "";
            comboBox10.Text = "";
            comboBox11.Text = "";
            comboBox12.Text = "";
        }

        private void textBox22_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                con.Open();
                setDataGridView9byName(textBox22.Text);
                con.Close();
            }
        }

        private void setDataGridView9byName(string p)
        {
            SqlCommand command;
            dataGridView9.DataSource = null;
            DataTable table = new DataTable();
            SqlDataAdapter adapter = null;
            command = new SqlCommand("select nome, id from pf_membro_da_staff where nome like '%" + p + "%';", con);
            adapter = new SqlDataAdapter(command);
            adapter.Fill(table);
            dataGridView9.DataSource = table;
            dataGridView9.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView9.ColumnHeadersVisible = true;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            
            try
            {
                con.Open();
                string nome = textBox23.Text;
                int idade = Convert.ToInt16(textBox24.Text);
                string sexo = "";
                if (radioButton1.Checked)
                    sexo = "Male";
                else
                    sexo = "Female";
                SqlCommand com = new SqlCommand("add_Actor", con)
                {
                    CommandType = CommandType.StoredProcedure
                };
                com.Parameters.AddWithValue("@name", nome);
                com.Parameters.AddWithValue("@age", idade);
                com.Parameters.AddWithValue("@gender", sexo);
                com.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                errorProvider1.SetError(button8,"Invalid data!");
            }
            finally
            {
                con.Close();
                textBox23.Text = "";
                textBox24.Text = "";
                radioButton1.Checked = false;
                radioButton2.Checked = false;
                setDataGridView9();
                setcombobox3();
                setcombobox4();
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();

            try
            {
                con.Open();
                string nome = textBox23.Text;
                int idade = Convert.ToInt16(textBox24.Text);
                string sexo = "";
                if (radioButton1.Checked)
                    sexo = "Male";
                else
                    sexo = "Female";
                SqlCommand com = new SqlCommand("add_Filmmaker", con)
                {
                    CommandType = CommandType.StoredProcedure
                };
                com.Parameters.AddWithValue("@name", nome);
                com.Parameters.AddWithValue("@age", idade);
                com.Parameters.AddWithValue("@gender", sexo);
                com.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                errorProvider1.SetError(button9, "Invalid data!");
            }
            finally
            {
                con.Close();
                textBox23.Text = "";
                textBox24.Text = "";
                radioButton1.Checked = false;
                radioButton2.Checked = false;
                setDataGridView9();
                setcombobox3();
                setcombobox4();
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            con.Open();
            SqlCommand com = new SqlCommand("remove_Cast_Member", con)
            {
                CommandType = CommandType.StoredProcedure
            };
            com.Parameters.AddWithValue("@id", Convert.ToInt16(dataGridView9.SelectedRows[0].Cells[1].Value.ToString()));
            com.ExecuteNonQuery();
            try
            {
              
            }
            catch (Exception ex)
            {
                errorProvider1.SetError(button10, "Select the cast member you want to remove!");
            }
            finally
            {
                con.Close();
                setDataGridView9();
                setcombobox3();
                setcombobox4();
            }
        }

      
    }
}
