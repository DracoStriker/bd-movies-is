﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace GestorDeCinemas
{
    public partial class EditProfile : Form
    {
        private SqlConnection con;
        private Home homeWindow;
        private string username;

        public EditProfile(string username, Home homeWindow)
        {
            InitializeComponent();
            con = new SqlConnection(MovieManager.Database);
            this.username = username;
            this.homeWindow = homeWindow;
            profileBox.Text = "Edit your profile here.";
            profilePassBox.Text = "Change your pass here.";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlCommand command;
            int age = 0;
            int bi = 0;
            int nif = 0;
            string gender = "";
            profileBox.Text = "";
            profilePassBox.Text = "";
            if (!String.IsNullOrEmpty(ageBox.Text))
            {
                if (!int.TryParse(ageBox.Text, out age))
                {
                    errorProvider1.Clear();
                    errorProvider1.SetError(ageBox, "Insert a numeric value.");
                    profileBox.Text = "Edit your profile here.";
                    profilePassBox.Text = "Change your pass here.";
                    return;
                }
                if (age < 0)
                {
                    errorProvider1.Clear();
                    errorProvider1.SetError(ageBox, "Insert a positive value.");
                    profileBox.Text = "Edit your profile here.";
                    profilePassBox.Text = "Change your pass here.";
                    return;
                }
            }
            if (!String.IsNullOrEmpty(biBox.Text))
            {
                if (!int.TryParse(biBox.Text, out bi))
                {
                    errorProvider1.Clear();
                    errorProvider1.SetError(biBox, "Insert a numeric value.");
                    profileBox.Text = "Edit your profile here.";
                    profilePassBox.Text = "Change your pass here.";
                    return;
                }
                if (!(bi > 9999999 && bi < 100000000))
                {
                    errorProvider1.Clear();
                    errorProvider1.SetError(biBox, "Insert a valid value.");
                    profileBox.Text = "Edit your profile here.";
                    profilePassBox.Text = "Change your pass here.";
                    return;
                }
            }
            if (!String.IsNullOrEmpty(nifBox.Text))
            {
                if (!int.TryParse(nifBox.Text, out nif))
                {
                    errorProvider1.Clear();
                    errorProvider1.SetError(nifBox, "Insert a numeric value.");
                    profileBox.Text = "Edit your profile here.";
                    profilePassBox.Text = "Change your pass here.";
                    return;
                }
                if (!(nif > 99999999 && nif < 1000000000))
                {
                    errorProvider1.Clear();
                    errorProvider1.SetError(nifBox, "Insert a valid value.");
                    profileBox.Text = "Edit your profile here.";
                    profilePassBox.Text = "Change your pass here.";
                    return;
                }
            }
            command = new SqlCommand("validate_user_update", con)
            {
                CommandType = CommandType.StoredProcedure
            };
            command.Parameters.Add(new SqlParameter("@username", username));
            command.Parameters.Add(new SqlParameter("@nome", nameBox.Text));
            if (!String.IsNullOrEmpty(ageBox.Text))
            {
                command.Parameters.Add(new SqlParameter("@idade", SqlDbType.Int)
                {
                    Value = age
                });
            }
            if (maleButton.Checked)
            {
                command.Parameters.Add(new SqlParameter("@sexo", "Male"));
                gender = "Male";
            }
            else if (femaleButton.Checked)
            {
                command.Parameters.Add(new SqlParameter("@sexo", "Female"));
                gender = "Female";
            }
            if (!String.IsNullOrEmpty(biBox.Text))
            {
                command.Parameters.Add(new SqlParameter("@bi", SqlDbType.Int)
                {
                    Value = bi
                });
            }
            if (!String.IsNullOrEmpty(nifBox.Text))
            {
                command.Parameters.Add(new SqlParameter("@nif", SqlDbType.Int)
                {
                    Value = nif
                });
            }
            errorProvider1.Clear();
            con.Open();
            command.ExecuteNonQuery();
            con.Close();
            homeWindow.fillInProfile(nameBox.Text, ageBox.Text, gender, biBox.Text, nifBox.Text);
            profileBox.Text = "Profile changed.";
        }

        private void EditProfile_Load(object sender, EventArgs e)
        {
            SqlCommand command;
            SqlDataReader reader;
            con.Open();
            command = new SqlCommand("select * from pf_membro;", con);
            reader = command.ExecuteReader();
            while (reader.Read())
            {
                if (reader["username"].ToString() == username)
                {
                    nameBox.Text = reader["nome"].ToString();
                    ageBox.Text = reader["idade"].ToString();
                    switch (reader["sexo"].ToString())
                    {
                        case "Male":
                            maleButton.Checked = true;
                            femaleButton.Checked = false;
                            break;
                        case "Female":
                            maleButton.Checked = false;
                            femaleButton.Checked = true;
                            break;
                        default:
                            maleButton.Checked = false;
                            femaleButton.Checked = false;
                            break;
                    }
                    biBox.Text = reader["bi"].ToString();
                    nifBox.Text = reader["nif"].ToString();
                    break;
                }
            }
            reader.Close();
            con.Close();
        }

        private void savePassButton_Click(object sender, EventArgs e)
        {
            SqlCommand command;
            profileBox.Text = "";
            profilePassBox.Text = "";
            if (String.IsNullOrEmpty(oldPassBox.Text))
            {
                errorProvider1.Clear();
                errorProvider1.SetError(oldPassBox, "Insert old password.");
                profileBox.Text = "Edit your profile here.";
                profilePassBox.Text = "Change your pass here.";
                return;
            }
            if (String.IsNullOrEmpty(newPassBox.Text))
            {
                errorProvider1.Clear();
                errorProvider1.SetError(newPassBox, "Insert new password.");
                profileBox.Text = "Edit your profile here.";
                profilePassBox.Text = "Change your pass here.";
                return;
            }
            if (String.IsNullOrEmpty(confirmPassBox.Text))
            {
                errorProvider1.Clear();
                errorProvider1.SetError(confirmPassBox, "Confirm the new password.");
                profileBox.Text = "Edit your profile here.";
                profilePassBox.Text = "Change your pass here.";
                return;
            }
            if (newPassBox.Text != confirmPassBox.Text)
            {
                errorProvider1.Clear();
                errorProvider1.SetError(confirmPassBox, "New and confirmation password don't match.");
                profileBox.Text = "Edit your profile here.";
                profilePassBox.Text = "Change your pass here.";
                return;
            }
            command = new SqlCommand("validate_pwd_reset", con)
            {
                CommandType = CommandType.StoredProcedure
            };
            command.Parameters.Add(new SqlParameter("@username", username));
            command.Parameters.Add(new SqlParameter("@pwd", oldPassBox.Text));
            command.Parameters.Add(new SqlParameter("@new_pwd", newPassBox.Text));
            command.Parameters.Add(new SqlParameter("@return", SqlDbType.VarChar)
            {
                Direction = ParameterDirection.Output,
                Size = 16
            });
            errorProvider1.Clear();
            con.Open();
            command.ExecuteNonQuery();
            if (command.Parameters["@return"].Value.ToString() == "False")
            {
                errorProvider1.SetError(oldPassBox, "Invalid password.");
            }
            con.Close();
            profilePassBox.Text = "Password changed.";
        }
    }
}
