﻿namespace GestorDeCinemas
{
    partial class Session
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel2 = new System.Windows.Forms.Panel();
            this.seatsGrid = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.rowBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.columnBox = new System.Windows.Forms.TextBox();
            this.addToCartButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cinemaBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.priceBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.roomBox = new System.Windows.Forms.TextBox();
            this.movieBox = new System.Windows.Forms.TextBox();
            this.startHourBox = new System.Windows.Forms.TextBox();
            this.dateBox = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.messageBox = new System.Windows.Forms.TextBox();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.seatsGrid)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.seatsGrid);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(179, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(231, 214);
            this.panel2.TabIndex = 63;
            // 
            // seatsGrid
            // 
            this.seatsGrid.AllowUserToAddRows = false;
            this.seatsGrid.AllowUserToDeleteRows = false;
            this.seatsGrid.AllowUserToResizeColumns = false;
            this.seatsGrid.AllowUserToResizeRows = false;
            this.seatsGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.seatsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.seatsGrid.Location = new System.Drawing.Point(16, 41);
            this.seatsGrid.Name = "seatsGrid";
            this.seatsGrid.ReadOnly = true;
            this.seatsGrid.RowHeadersVisible = false;
            this.seatsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.seatsGrid.Size = new System.Drawing.Size(200, 158);
            this.seatsGrid.TabIndex = 62;
            this.seatsGrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView6_CellClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 61;
            this.label1.Text = "Available Seats";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(199, 244);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 66;
            this.label4.Text = "Row";
            // 
            // rowBox
            // 
            this.rowBox.Location = new System.Drawing.Point(247, 241);
            this.rowBox.Name = "rowBox";
            this.rowBox.ReadOnly = true;
            this.rowBox.Size = new System.Drawing.Size(148, 20);
            this.rowBox.TabIndex = 65;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(199, 275);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 67;
            this.label5.Text = "Column";
            // 
            // columnBox
            // 
            this.columnBox.Location = new System.Drawing.Point(247, 272);
            this.columnBox.Name = "columnBox";
            this.columnBox.ReadOnly = true;
            this.columnBox.Size = new System.Drawing.Size(148, 20);
            this.columnBox.TabIndex = 68;
            // 
            // addToCartButton
            // 
            this.addToCartButton.Location = new System.Drawing.Point(12, 269);
            this.addToCartButton.Name = "addToCartButton";
            this.addToCartButton.Size = new System.Drawing.Size(80, 23);
            this.addToCartButton.TabIndex = 64;
            this.addToCartButton.Text = "Add to Cart";
            this.addToCartButton.UseVisualStyleBackColor = true;
            this.addToCartButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel1.Controls.Add(this.cinemaBox);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.priceBox);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.roomBox);
            this.panel1.Controls.Add(this.movieBox);
            this.panel1.Controls.Add(this.startHourBox);
            this.panel1.Controls.Add(this.dateBox);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(161, 214);
            this.panel1.TabIndex = 62;
            // 
            // cinemaBox
            // 
            this.cinemaBox.Location = new System.Drawing.Point(73, 179);
            this.cinemaBox.Name = "cinemaBox";
            this.cinemaBox.ReadOnly = true;
            this.cinemaBox.Size = new System.Drawing.Size(71, 20);
            this.cinemaBox.TabIndex = 60;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 182);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 59;
            this.label3.Text = "Cinema";
            // 
            // priceBox
            // 
            this.priceBox.Location = new System.Drawing.Point(73, 144);
            this.priceBox.Name = "priceBox";
            this.priceBox.ReadOnly = true;
            this.priceBox.Size = new System.Drawing.Size(71, 20);
            this.priceBox.TabIndex = 58;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 147);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 57;
            this.label2.Text = "Price";
            // 
            // roomBox
            // 
            this.roomBox.Location = new System.Drawing.Point(73, 10);
            this.roomBox.Name = "roomBox";
            this.roomBox.ReadOnly = true;
            this.roomBox.Size = new System.Drawing.Size(71, 20);
            this.roomBox.TabIndex = 56;
            // 
            // movieBox
            // 
            this.movieBox.Location = new System.Drawing.Point(73, 41);
            this.movieBox.Name = "movieBox";
            this.movieBox.ReadOnly = true;
            this.movieBox.Size = new System.Drawing.Size(71, 20);
            this.movieBox.TabIndex = 55;
            // 
            // startHourBox
            // 
            this.startHourBox.Location = new System.Drawing.Point(73, 75);
            this.startHourBox.Name = "startHourBox";
            this.startHourBox.ReadOnly = true;
            this.startHourBox.Size = new System.Drawing.Size(71, 20);
            this.startHourBox.TabIndex = 54;
            // 
            // dateBox
            // 
            this.dateBox.Location = new System.Drawing.Point(73, 109);
            this.dateBox.Name = "dateBox";
            this.dateBox.ReadOnly = true;
            this.dateBox.Size = new System.Drawing.Size(71, 20);
            this.dateBox.TabIndex = 53;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(13, 13);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(50, 13);
            this.label17.TabIndex = 48;
            this.label17.Text = "Room Nº";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(13, 44);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(36, 13);
            this.label21.TabIndex = 49;
            this.label21.Text = "Movie";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(13, 78);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(55, 13);
            this.label22.TabIndex = 50;
            this.label22.Text = "Start Hour";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(13, 112);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(30, 13);
            this.label23.TabIndex = 51;
            this.label23.Text = "Date";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // messageBox
            // 
            this.messageBox.Location = new System.Drawing.Point(12, 241);
            this.messageBox.Name = "messageBox";
            this.messageBox.ReadOnly = true;
            this.messageBox.Size = new System.Drawing.Size(161, 20);
            this.messageBox.TabIndex = 69;
            // 
            // Session
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(419, 305);
            this.Controls.Add(this.messageBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.rowBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.columnBox);
            this.Controls.Add(this.addToCartButton);
            this.Name = "Session";
            this.Text = "Session";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.seatsGrid)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView seatsGrid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox roomBox;
        private System.Windows.Forms.TextBox movieBox;
        private System.Windows.Forms.TextBox startHourBox;
        private System.Windows.Forms.TextBox dateBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button addToCartButton;
        private System.Windows.Forms.TextBox priceBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox cinemaBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox rowBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox columnBox;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.TextBox messageBox;
    }
}