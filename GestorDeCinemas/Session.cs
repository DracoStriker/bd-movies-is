﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace GestorDeCinemas
{
    public partial class Session : Form
    {
        private SqlConnection con;
        private string hora_da_sessao;
        private string data;
        private string numero_da_sala;
        private string localizacao_da_sala;

        public Session(string hora_da_sessao, string data, string numero_da_sala, string localizacao_da_sala)
        {
            InitializeComponent();
            con = new SqlConnection(MovieManager.Database);
            this.hora_da_sessao = hora_da_sessao;
            this.data = data;
            this.numero_da_sala = numero_da_sala;
            this.localizacao_da_sala = localizacao_da_sala;
            messageBox.Text = "Select a seat.";
            con.Open();
            setContents();
            con.Close();
        }

        private void setContents()
        {
            seatsGrid.DataSource = null;
            SqlCommand command;
            DataTable table;
            SqlDataAdapter adapter;
            table = new DataTable();
            command = new SqlCommand("get_session", con)
            {
                CommandType = CommandType.StoredProcedure
            };
            command.Parameters.Add(new SqlParameter("@hora_da_sessao", SqlDbType.Time)
            {
                Value = hora_da_sessao
            });
            command.Parameters.Add(new SqlParameter("@data", SqlDbType.Date)
            {
                Value = data
            });
            command.Parameters.Add(new SqlParameter("@numero_da_sala", SqlDbType.Int)
            {
                Value = Convert.ToInt32(numero_da_sala)
            });
            command.Parameters.Add(new SqlParameter("@localizacao_da_sala", SqlDbType.VarChar)
            {
                Value = localizacao_da_sala,
                Size = 32
            });
            command.Parameters.Add(new SqlParameter("@filme", SqlDbType.VarChar)
            {
                Direction = ParameterDirection.Output,
                Size = 16
            });
            command.Parameters.Add(new SqlParameter("@price", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            });
            adapter = new SqlDataAdapter(command);
            adapter.Fill(table);
            seatsGrid.DataSource = table;
            seatsGrid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            roomBox.Text = numero_da_sala;
            movieBox.Text = command.Parameters["@filme"].Value.ToString();
            startHourBox.Text = hora_da_sessao;
            dateBox.Text = data;
            priceBox.Text = command.Parameters["@price"].Value.ToString();
            cinemaBox.Text = localizacao_da_sala;
            rowBox.Text = "";
            columnBox.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {

            Ticket ticket;
            if ((rowBox.Text != "") && (columnBox.Text != ""))
            {
                ticket = new Ticket(
                         Convert.ToInt32(priceBox.Text),
                         dateBox.Text,
                         startHourBox.Text,
                         Convert.ToInt32(roomBox.Text),
                         cinemaBox.Text,
                         Convert.ToChar(rowBox.Text),
                         Convert.ToInt32(columnBox.Text));
                foreach (Ticket t in MovieManager.ReservationList)
                {
                    if(ticket == t)
                    {
                        errorProvider1.Clear();
                        errorProvider1.SetError(addToCartButton, "Already added to cart.");
                        messageBox.Text = "Select another seat.";
                        return;
                    }
                }
                errorProvider1.Clear();
                MovieManager.ReservationList.Add(ticket);
                messageBox.Text = "Seat added to cart.";
           }
           else
           {
                errorProvider1.Clear();
                errorProvider1.SetError(addToCartButton, "You must select a seat.");
           }
        }

        private void dataGridView6_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (seatsGrid.RowCount > 0)
            {
                rowBox.Text = seatsGrid.SelectedRows[0].Cells[0].Value.ToString();
                columnBox.Text = seatsGrid.SelectedRows[0].Cells[1].Value.ToString();
            }
            else
            {
                errorProvider1.Clear();
                errorProvider1.SetError(addToCartButton, "No tickets available.");
            }
            messageBox.Text = "Select a seat.";
        }
    }
}