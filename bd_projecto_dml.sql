
-- Queries para debbug

/*
select * from 
select * from pf_reserva;
select * from pf_membro;
select * from pf_amigo;
select * from pf_bilhete;
select * from pf_cinema;
select * from pf_sala;
select * from pf_lugar;
select * from pf_sessao;
select * from pf_filme;
select * from pf_genero_filme;
select * from pf_produtora;
select * from pf_membro_da_staff;
select * from pf_actua;
select * from pf_actor;
select * from pf_realizador;
*/

-- Login Queries

go
if object_id('validate_registry') is not null
	drop proc validate_registry

go
create proc validate_registry @username varchar(16), @pwd varchar(16), @return varchar(5) output as
	select *
	from pf_membro
	where username = @username
	if(@@rowcount = 0)
		begin
			insert into pf_membro (username, pwd) values (@username, encryptbypassphrase('adivinha_esta',@pwd))
			set @return = 'True'
		end
	else
		set @return = 'False'

go
if object_id('validate_login') is not null
	drop proc validate_login

go
create proc validate_login @username varchar(16), @pwd varchar(16), @return varchar(5) output as
	if(exists(	select *
				from pf_membro
				where username = @username and cast (decryptbypassphrase('adivinha_esta',pwd) as varchar(16)) = @pwd))
		set @return = 'True'
	else
		set @return = 'False'

-- Edit Profile Queries

go
if object_id('validate_user_update') is not null
	drop proc validate_user_update

go
create proc validate_user_update @username varchar(16), @nome varchar(32) = null, @idade int = null, @sexo varchar(6) = null, @bi int = null, @nif int = null as
	update pf_membro
	set username = @username, nome = @nome, idade = @idade, sexo = @sexo, bi = @bi, nif = @nif
	where username = @username

go
if object_id('validate_pwd_reset') is not null
	drop proc validate_pwd_reset

go
create proc validate_pwd_reset @username varchar(16), @pwd varchar(16), @new_pwd varchar(16), @return varchar(5) output as
	update pf_membro
	set pwd = encryptbypassphrase('adivinha_esta',@new_pwd)
	where username = @username and cast(decryptbypassphrase('adivinha_esta',pwd) as varchar(16)) = @pwd
	if (@@rowcount = 0)
		set @return = 'False'
	else
		set @return = 'True'

-- Movie Queries

go
if object_id('generos_do_filme') is not null
	drop proc generos_do_filme

go
create proc generos_do_filme @nome_filme varchar(32), @nome_produtora varchar(32) as
	select genero
	from pf_filme join pf_genero_filme on nome_do_filme = nome and pf_filme.nome_da_produtora = pf_genero_filme.nome_da_produtora
	where nome = @nome_filme and pf_filme.nome_da_produtora = @nome_produtora

go
if object_id('nome_actor') is not null
	drop function nome_actor

go
create function nome_actor (@id_actor int) returns varchar(32) as
begin
	declare @nome varchar(32)
	select @nome = nome
	from pf_membro_da_staff join pf_actor on id_de_membro_da_staff = id
	where id = @id_actor
	return @nome
end

go
if object_id('actores_do_filme') is not null
	drop proc actores_do_filme

go
create proc actores_do_filme @nome_filme varchar(32), @nome_produtora varchar(32) as
	select dbo.nome_actor (temp.id_do_actor) as nome, temp.papel as papel
	from (	select id_do_actor, papel
			from pf_actua join pf_filme on nome_do_filme = nome and pf_filme.nome_da_produtora = pf_actua.produtora_do_filme
			where nome = @nome_filme and pf_filme.nome_da_produtora = @nome_produtora) as temp

go
if object_id('amigos_membro') is not null
	drop proc amigos_membro

go
create proc amigos_membro @username varchar(16) as
	select user_do_membro2 as Amigo
	from pf_amigo
	where user_do_membro1 = @username

go
if object_id('recomendar') is not null
	drop proc recomendar

go
create proc recomendar @username varchar(16), @amigo varchar(16), @filme varchar(32), @produtora varchar(32), @return varchar(5) output as
	if (exists( select *
				from pf_recomendacoes
				where friend_user = @username and username = @amigo and nome_do_filme = @filme and nome_da_produtora = @produtora))
	set @return = 'False'
	else
		begin
			insert into pf_recomendacoes (username,friend_user,nome_do_filme,nome_da_produtora) values (@amigo,@username,@filme,@produtora)
			set @return = 'True'
		end


-- Reservation Queries

go
if object_id('get_reservation') is not null
	drop proc get_reservation

go
create proc get_reservation @id int, @date date output, @nTickets int output, @totalPrice int output as
	select nome_do_filme, produtora_do_filme, temp.preco, temp.data, temp.hora_da_sessao, temp.numero_da_sala, temp.localizacao_da_sala, temp.fila_do_lugar, temp.coluna_do_lugar
	from pf_sessao join (	select preco, data, hora_da_sessao, numero_da_sala, localizacao_da_sala, fila_do_lugar, coluna_do_lugar
							from pf_bilhete
							where id_da_compra = @id
						) as temp on hora_da_sessao = hora_de_inicio and pf_sessao.data = temp.data and pf_sessao.numero_da_sala = temp.numero_da_sala and pf_sessao.localizacao_da_sala = temp.localizacao_da_sala
	select @date = pf_reserva.data, @nTickets = count(*) , @totalPrice = sum(preco)
	from pf_reserva join pf_bilhete on pf_bilhete.id_da_compra = pf_reserva.id
	where pf_reserva.id = @id
	group by pf_reserva.data, pf_reserva.user_do_membro

-- Session Queries

go
if object_id('get_session') is not null
	drop proc get_session

go
create proc get_session @hora_da_sessao time, @data date, @numero_da_sala int, @localizacao_da_sala varchar(32), @filme varchar(32) output, @price int output as
	select fila, coluna
	from	(	select fila_do_lugar, coluna_do_lugar
				from pf_bilhete
				where hora_da_sessao = @hora_da_sessao and data = @data and numero_da_sala = @numero_da_sala and localizacao_da_sala = @localizacao_da_sala) as bilhetes
	right outer join 
			(	select fila, coluna
				from pf_lugar
				where numero_da_sala = @numero_da_sala and localizacao_da_sala = @localizacao_da_sala) as lugares
	on fila_do_lugar = fila and coluna_do_lugar = coluna
	where fila_do_lugar is null and coluna_do_lugar is null
	select @filme = nome_do_filme, @price = preco
	from pf_sessao
	where hora_de_inicio = @hora_da_sessao and data = @data and numero_da_sala = @numero_da_sala and localizacao_da_sala = @localizacao_da_sala

-- Home Queries

go
if object_id('get_Cast_Info') is not null
	drop proc get_Cast_Info

go
create proc get_Cast_Info @id int as
	select  nome, idade,sexo  from pf_membro_da_staff where id = @id;

go
if object_id('get_Filmmaker') is not null
	drop proc get_Filmmaker

go
create proc get_Filmmaker @id int as
	select * from pf_Realizador where id_de_membro_da_staff = @id;

go
if object_id('get_Actors') is not null
	drop proc get_Actors

go
create proc get_Actors @id int as
	select nome_do_filme as Movie, produtora_do_filme as Producer from pf_actua where id_do_actor = @id;

go
if object_id('get_Filmmakers') is not null
	drop proc get_Filmmakers

go
create proc get_Filmmakers @ii int as
	select nome as Movie, nome_da_produtora as Producer from pf_filme where id_do_realizador = @ii;

go
if object_id('add_Friend') is not null
	drop proc add_Friend

go
create proc add_Friend @user1 varchar(16), @user2 varchar(16)
as
	insert into pf_amigo values(@user1,@user2);
	insert into pf_amigo values(@user2,@user1);

go
if object_id('remove_Friend') is not null
	drop proc remove_Friend

go
create proc remove_Friend @user1 varchar(16), @user2 varchar(16)
as
	delete from pf_amigo where user_do_membro1 = @user1 and user_do_membro2 = @user2;
	delete from pf_amigo where user_do_membro1 = @user2 and user_do_membro2 = @user1;

go
if object_id('search_Friend') is not null
	drop proc search_Friend

go
create proc search_Friend @user1 varchar(16), @user2 varchar(16)
as
	select user_do_membro2 as Friend from pf_amigo where user_do_membro1 = @user1 and user_do_membro2 like '%'+@user2+'%';

go
if object_id('search_member_by_Name') is not null
	drop proc search_member_by_Name

go
create proc search_member_by_Name @name varchar(16)
as
	select username, nome from pf_membro where username like '%'+@name+'%' or nome like '%' +@name+'%';

go
if object_id('remove_Recommendation') is not null
	drop proc remove_Recommendation

go
create proc remove_Recommendation @user varchar(16), @movie varchar(32), @prod varchar(32)
as
	delete from pf_recomendacoes where username = @user and nome_do_filme = @movie and nome_da_produtora = @prod;


go
if object_id('get_max_ReservaID') is not null
	drop proc get_max_ReservaID

go
create proc get_max_ReservaID as
	select max(id) as maximo from pf_reserva;


go
if object_id('add_Bilhete') is not null
	drop proc add_Bilhete

go
create proc add_Bilhete @price int, @data date, @hora time(7), @sala int, 
						 @cinema varchar(32), @fila char(1), @col int, @id int
as
	insert into pf_bilhete values(@price,@data,@hora,@sala,@cinema,@fila,@col,@id);

-- Managment Queries
go
if object_id('get_Cinemas') is not null
	drop proc get_Cinemas

go
create proc get_Cinemas
as
select * from pf_cinema


go
if object_id('get_Rooms_from_Cinema') is not null
	drop proc get_Rooms_from_Cinema

go
create proc get_Rooms_from_Cinema @nome_Cinema varchar(32)
as
select numero as Sala from pf_sala where localizacao_do_cinema = @nome_Cinema

go
if object_id('add_Cinema') is not null
	drop proc add_Cinema

go
create proc add_Cinema @name varchar(32)
as
	insert into pf_cinema values(@name);

go
if object_id('add_New_Room') is not null
	drop proc add_New_Room

go
create proc add_New_Room @cinema varchar(32) as
begin
	declare @num int
	declare @x int
	select @x=max(numero) from pf_sala where @cinema = localizacao_do_cinema
	if (@x is null)
	begin
		set @num = 1
		print 'ola'
	end
	else
	begin
		print 'ole'
		set @num = @x + 1
	end
	insert into pf_sala values(@num,@cinema)
end

go
if object_id('get_Cinemas_by_Name') is not null
	drop proc get_Cinemas_by_Name

go
create proc get_Cinemas_by_Name @name varchar(32) as
	select localizacao as Cinema from pf_cinema where localizacao like '%'+@name+'%'

go
if object_id('get_Movies_by_Name') is not null
	drop proc get_Movies_by_Name

go
create proc get_Movies_by_Name @name varchar(32) as
	select nome as Name, nome_da_produtora as Producer from pf_filme where nome like '%'+@name+'%'

go
if object_id('get_Movies') is not null
	drop proc get_Movies

go
create proc get_Movies
as
select nome as Name, nome_da_produtora as Producer from pf_filme


go
if object_id('add_Gender') is not null
	drop proc add_Gender

go
create proc add_Gender @movie varchar(32), @producer varchar(32), @gen varchar(32) as
		insert into pf_genero_filme values(@movie,@producer,@gen)


go
if object_id('get_Genders') is not null
	drop proc get_Genders

go 
create proc get_Genders @movie varchar(32), @prod varchar(32) as
	select distinct genero from pf_genero_filme where nome_do_filme = @movie and nome_da_produtora = @prod

go
if object_id('remove_Gender') is not null
	drop proc remove_Gender

go 
create proc remove_Gender @movie varchar(32), @prod varchar(32), @gen varchar(32)
as
	delete from pf_genero_filme  where nome_do_filme = @movie and nome_da_produtora = @prod and genero = @gen;
go
if object_id('add_Movie') is not null
	drop proc add_Movie

go
create proc add_Movie @movie varchar(32), @prod varchar(32)
as
	insert into pf_filme (nome,nome_da_produtora) values (@movie,@prod);

go
if object_id('add_Actor_To_Movie') is not null
	drop proc add_Actor_To_Movie

go
create proc add_Actor_To_Movie @movie varchar(32), @prod varchar(32), @actor int, @role varchar(32)
as
	insert into pf_actua values (@actor,@movie,@prod,@role)

go
if object_id('update_Movie') is not null
	drop proc update_Movie

go
create proc update_Movie @movie varchar(32), @prod varchar(32), 
						@time time(7), @filmmaker int, @description varchar(256),
						@year int 
as
	update pf_filme 
	set descricao = @description, ano = @year, duracao = @time, id_do_realizador = @filmmaker
	where nome = @movie and nome_da_produtora = @prod;

go
if object_id('add_Session') is not null
	drop proc add_Session

go
create proc add_Session @movie varchar(32), @prod varchar(32), @hour time(7), @date date, @sala int, @cinema varchar(32), @price int
as
	declare @count int;
	select @count = count(*) from pf_sessao
							where hora_de_inicio = @hour and data = @date and 
								numero_da_sala=@sala and localizacao_da_sala = @cinema;
	if(@count = 0)
	begin
		insert into pf_sessao values(@hour,@date,@sala,@movie,@prod,@cinema,@price);
	end
	else
	begin
		update pf_sessao
		set nome_do_filme = @movie, produtora_do_filme = @prod, preco = @price
		where hora_de_inicio = @hour and data = @date and 
								numero_da_sala=@sala and localizacao_da_sala = @cinema;
	end


go
if object_id('add_Actor') is not null
	drop proc add_Actor

go
create proc add_Actor @name varchar(32), @age int, @gender varchar(6) as
	begin tran
	declare @id int;
	select @id = max(id) from pf_membro_da_staff;
	set @id +=1;
	insert into pf_membro_da_staff values(@name,@age,@gender,@id);
	insert into pf_actor values(@id);
	commit tran

go
if object_id('add_Filmmaker') is not null
	drop proc add_Filmmaker

go
create proc add_Filmmaker @name varchar(32), @age int, @gender varchar(6) as
	begin tran
	declare @id int;
	select @id = max(id) from pf_membro_da_staff;
	set @id +=1;
	insert into pf_membro_da_staff values(@name,@age,@gender,@id);
	insert into pf_realizador values(@id);
	commit tran

go
if object_id('remove_Cast_Member') is not null
	drop proc remove_Cast_Member

go
create proc remove_Cast_Member @id int as
	begin tran;
	update pf_filme set id_do_realizador = null where id_do_realizador = @id;
	delete from pf_actua where id_do_actor = @id;
	delete from pf_actor where id_de_membro_da_staff = @id;
	delete from pf_realizador where id_de_membro_da_staff = @id;
	delete from pf_membro_da_staff where id=@id;
	commit tran;