
-- Limpeza das tabelas

go
if object_id('pf_recomendacoes') is not null
	drop table pf_recomendacoes

if object_id('pf_amigo') is not null
	drop table pf_amigo

if object_id('pf_actua') is not null
	drop table pf_actua

if object_id('pf_actor') is not null
	drop table pf_actor

if object_id('filme_realizador') is not null
	alter table pf_filme
		drop constraint filme_realizador

if object_id('pf_realizador') is not null
	drop table pf_realizador

if object_id('pf_membro_da_staff') is not null
	drop table pf_membro_da_staff

if object_id('pf_bilhete') is not null
	drop table pf_bilhete

if object_id('pf_sessao') is not null
	drop table pf_sessao

if object_id('pf_genero_filme') is not null
	drop table pf_genero_filme

if object_id('pf_filme') is not null
	drop table pf_filme

if object_id('pf_lugar') is not null
	drop table pf_lugar

if object_id('pf_sala') is not null
	drop table pf_sala

if object_id('reserva_membro') is not null
	alter table pf_reserva
		drop constraint reserva_membro

if object_id('pf_membro') is not null
	drop table pf_membro

if object_id('pf_produtora') is not null
	drop table pf_produtora

if object_id('pf_reserva') is not null
	drop table pf_reserva

if object_id('pf_cinema') is not null
	drop table pf_cinema

-- Tabela Cinemas

go
create table pf_cinema (
	localizacao varchar(32) primary key not null
);

go
if object_id('block_cinema') is not null
	drop trigger block_cinema

go
create trigger block_cinema on pf_cinema instead of update, delete as
	begin
		print('Os cinemas n�o podem ser modificados ou eliminados!')
	end

go
if object_id('on_adding_cinema') is not null
	drop trigger block_cinon_adding_cinemaema

go 
create trigger on_adding_cinema on dbo.pf_cinema after insert as
begin
    declare @cin as varchar(32);
	select @cin=localizacao from inserted;
	exec add_New_Room @cin;		
end

-- Tabela Reservas

go
create table pf_reserva (
	data date,
	id int primary key check(id >= 0) not null,
	user_do_membro varchar(16) not null
);

go
if object_id('block_reserva') is not null
	drop trigger block_reserva

go
create trigger block_reserva on pf_reserva instead of update, delete as
	begin
		print('As reservas n�o podem ser modificadas ou eliminadas!')
	end

-- Tabela Produtoras

go
create table pf_produtora (
	nome varchar(32) primary key not null
);

go
if object_id('block_produtora') is not null
	drop trigger block_produtora

go
create trigger block_produtora on pf_produtora  instead of update, delete as
	begin
		print('As produtoras de filmes n�o podem ser modificadas ou eliminadas!')
	end

-- Tabela Membros

go
create table pf_membro (
	username varchar(16) primary key not null,
	pwd varbinary(100) not null,
	nome varchar(32),
	idade int check(idade >= 0 and idade <= 100),
	sexo varchar(6) check (sexo in('Male','Female')),
	bi int check(bi > 9999999 and bi < 100000000),
	nif int check(nif > 99999999 and nif < 1000000000)
);

go
if object_id('block_membro') is not null
	drop trigger block_membro

go
create trigger block_membro on pf_membro instead of delete as
	begin
		print('Os membros n�o podem ser eliminados!')
	end

-- Tabela Reservas

go
alter table pf_reserva
	add constraint reserva_membro foreign key (user_do_membro) references pf_membro(username);

-- Tabela Salas

go
create table pf_sala (
	numero int check(numero > 0) not null,
	localizacao_do_cinema varchar(32) references pf_cinema(localizacao) not null,
	primary key (numero, localizacao_do_cinema)
);

go
if object_id('block_sala') is not null
	drop trigger block_sala

go
create trigger block_sala on pf_sala instead of update, delete as
	begin
		print('As salas n�o podem ser modificadas ou eliminadas!')
	end

go
if object_id('on_adding_sala') is not null
	drop trigger on_adding_sala

go
create trigger on_adding_sala on pf_sala after insert as
begin
	declare @cinema varchar(32);
	declare @sala int;
	select @sala = numero, @cinema = localizacao_do_cinema from inserted;
	declare @posF int;
	set @posF = 1;
	declare @maxposF int;
	set @maxposF = 16; 
	declare @fila char(16);
	set @fila = 'ABCDEFGHIJKLMNOPQ';
	declare @maxposC int;
	set @maxposC = 20;
	declare @posC int;

	while (@posF) <= (@maxposF)
	begin
		set @posC = 1;
		while (@posC) <= (@maxposC)
		begin
			insert into pf_lugar values(@sala,@cinema,char(ascii(substring(@fila,@posF,1))),@posC);
		set @posC += 1;
		end;
		set @posF += 1;
	end;
end


-- Tabela Lugares

go
create table pf_lugar (
	numero_da_sala int check(numero_da_sala > 0) not null,
	localizacao_da_sala varchar(32) not null,
	fila char not null,
	coluna int check(coluna > 0) not null,
	primary key (numero_da_sala, localizacao_da_sala, fila, coluna),
	foreign key (numero_da_sala, localizacao_da_sala) references pf_sala(numero, localizacao_do_cinema)
);

go
if object_id('block_lugar') is not null
	drop trigger block_lugar

go
create trigger block_lugar on pf_lugar instead of update, delete as
	begin
		print('Os lugares n�o podem ser modificados ou eliminados!')
	end

-- Tabela Filmes

go
create table pf_filme (
	nome varchar(32) not null,
	descricao varchar(256),
	ano int check(ano > 1900 and ano <= datepart(year,getdate())),
	duracao Time,
	nome_da_produtora varchar(32) references pf_produtora(nome) not null,
	id_do_realizador int,
	primary key (nome, nome_da_produtora)
);

go
if object_id('block_filme') is not null
	drop trigger block_filme

go
create trigger block_filme on pf_filme instead of delete as
	begin
		print('Os filmes n�o podem ser modificados ou eliminados!')
	end

go
if object_id('on_adding_Filme') is not null
	drop trigger on_adding_Filme

go
create trigger on_adding_Filme on pf_filme instead of insert as
begin
	declare @prod varchar(32);
	declare @exists int;
	select @prod = nome_da_produtora from inserted ;
	select @exists = count(*) from pf_produtora where nome = @prod;
	if(@exists = 0)
		insert into pf_produtora values(@prod);
	insert into pf_filme select * from inserted;
end

go
if object_id('idx_ano_filme') is not null
	drop index idx_ano_filme on pf_filme

create nonclustered index idx_ano_filme on pf_filme(ano) with(fillfactor=85)


go
if object_id('idx_realizador_filme') is not null
	drop index idx_realizador_filme on pf_filme

create nonclustered index idx_realizador_filme on pf_filme(id_do_realizador) with(fillfactor=85)

-- Tabela Generos de Filmes

go
create table pf_genero_filme (
	nome_do_filme varchar(32) not null,
	nome_da_produtora varchar(32) not null,
	genero varchar(32) check (genero in('Accao','Romance','Comedia','Aventura','Terror','Drama','Misterio')) not null,
	foreign key(nome_do_filme,nome_da_produtora) references pf_filme(nome, nome_da_produtora),
	primary key (nome_do_filme,nome_da_produtora, genero)
);

go
if object_id('idx_movie_gender') is not null
	drop index idx_movie_gender on pf_bilhete

create nonclustered index idx_movie_gender on pf_genero_filme(genero) with(fillfactor=85)

go
if object_id('block_genero_filme') is not null
	drop trigger block_genero_filme

go
create trigger block_genero_filme on pf_genero_filme instead of update as
	begin
		print('Os generos de filme n�o podem ser modificados!')
	end

-- Tabela Sessoes

go
create table pf_sessao (
	hora_de_inicio time not null,
	data date not null,
	numero_da_sala int check(numero_da_sala > 0) not null,
	nome_do_filme varchar(32) not null,
	produtora_do_filme varchar(32) not null,
	localizacao_da_sala varchar(32) not null,
	preco int not null,
	primary key (hora_de_inicio, data, numero_da_sala, localizacao_da_sala),
	foreign key (numero_da_sala, localizacao_da_sala) references pf_sala(numero, localizacao_do_cinema),
	foreign key (nome_do_filme, produtora_do_filme) references pf_filme(nome, nome_da_produtora)
);

go
if object_id('idx_session_date') is not null
	drop index idx_session_date on pf_bilhete

create nonclustered index idx_session_date on pf_sessao(data) with(fillfactor=85)

go
if object_id('idx_session_hour') is not null
	drop index idx_session_hour on pf_bilhete

create nonclustered index idx_session_hour on pf_sessao(hora_de_inicio) with(fillfactor=85)

go
if object_id('block_reserva') is not null
	drop trigger block_reserva

go
create trigger block_reserva on pf_reserva instead of update, delete as
	begin
		print('As reservas n�o podem ser modificadas ou eliminadas!')
	end

-- Tabela Bilhetes

go
create table pf_bilhete (
	preco int check(preco > 0) not null,
	data date not null,
	hora_da_sessao time not null,
	numero_da_sala int check(numero_da_sala > 0) not null,
	localizacao_da_sala varchar(32) not null,
	fila_do_lugar char not null,
	coluna_do_lugar int check(coluna_do_lugar > 0) not null,
	id_da_compra int check(id_da_compra > 0) not null,
	primary key (hora_da_sessao, data, numero_da_sala, localizacao_da_sala, fila_do_lugar, coluna_do_lugar),
	foreign key (numero_da_sala, localizacao_da_sala, fila_do_lugar, coluna_do_lugar) references pf_lugar (numero_da_sala, localizacao_da_sala, fila, coluna),
	foreign key (id_da_compra) references pf_reserva(id)
);

go
if object_id('block_bilhete') is not null
	drop trigger block_bilhete

go
create trigger block_bilhete on pf_bilhete instead of update, delete as
	begin
		print('Os bilhetes n�o podem ser modificados ou eliminados!')
	end

go
if object_id('idx_ticket_id') is not null
	drop index idx_ticket_id on pf_bilhete

create index idx_ticket_id on pf_bilhete(id_da_compra)

-- Tabela Membro da Producoes

go
create table pf_membro_da_staff (
	nome varchar(32) not null,
	idade int check(idade >= 0 and idade < =100),
	sexo varchar(6) check (sexo in('Male','Female')),
	id int primary key check(id >= 0)
);

go
if object_id('idx_staff_name') is not null
	drop index idx_staff_name on pf_membro_da_staff

create nonclustered index idx_staff_name on pf_membro_da_staff(nome) with (fillfactor = 85)

-- Tabela Realizadores

go
create table pf_realizador (
	id_de_membro_da_staff int primary key references pf_membro_da_staff(id) check(id_de_membro_da_staff >= 0) not null
);

go
alter table pf_filme
	add constraint filme_realizador foreign key (id_do_realizador) references pf_realizador(id_de_membro_da_staff)
	
-- Tabela Actores
	
go
create table pf_actor (
	id_de_membro_da_staff int primary key references pf_membro_da_staff(id) check(id_de_membro_da_staff >= 0) not null
);

-- Tabela Papeis
	
go
create table pf_actua (
	id_do_actor int references pf_actor(id_de_membro_da_staff) check (id_do_actor >= 0) not null,
	nome_do_filme varchar(32) not null,
	produtora_do_filme varchar(32) not null,
	papel varchar(32),
	primary key(id_do_actor, nome_do_filme, produtora_do_filme),
	foreign key (nome_do_filme, produtora_do_filme) references pf_filme(nome, nome_da_produtora)
);
	
go
if object_id('clean_actua') is not null
	drop trigger clean_actua

go
create trigger clean_actua on pf_actor after delete as
	declare @actor int
	select @actor = id_de_membro_da_staff
	from deleted
	delete from pf_actua
	where id_do_actor = @actor

-- Tabela Amigos

go
create table pf_amigo (
	user_do_membro1 varchar(16) references pf_membro(username) not null,
	user_do_membro2 varchar(16) references pf_membro(username) not null,
	primary key (user_do_membro1, user_do_membro2)
);

-- Tabela Recomendacoes

go
create table pf_recomendacoes (
	username varchar(16) references pf_membro(username) not null,
	friend_user varchar(16) references pf_membro(username) not null,
	nome_do_filme varchar(32) not null,
	nome_da_produtora varchar(32) not null,
	foreign key (nome_do_filme,nome_da_produtora) references pf_filme(nome,nome_da_produtora),
	primary key(username,friend_user,nome_do_filme,nome_da_produtora)
);

-- Inserts para debbug
go

-- Membros
insert into pf_membro values('John',encryptbypassphrase('adivinha_esta','abcd'),'Joao',23,'Male',12345677,123456788);
insert into pf_membro values('hugo',encryptbypassphrase('adivinha_esta','abcd'),'Hugo',20,'Male',12345671,123456782);
insert into pf_membro values('simon',encryptbypassphrase('adivinha_esta','abcd'),'Simao',21,'Male',12345676,123456787);
insert into pf_membro values('peter',encryptbypassphrase('adivinha_esta','abcd'),'Pedro',22,'Male',12345675,123456786);
insert into pf_membro values('Sarah',encryptbypassphrase('adivinha_esta','abcd'),'Sara',22,'Female',12345674,123456785);
insert into pf_membro values('Jessie',encryptbypassphrase('adivinha_esta','abcd'),'Jessica',35,'Female',12345673,123456784);
insert into pf_membro values('Charles',encryptbypassphrase('adivinha_esta','abcd'),'Carlos',30,'Male',12345672,123456783);

-- Amigos
insert into pf_amigo values('John','hugo');
insert into pf_amigo values('hugo','John');
insert into pf_amigo values('hugo','simon');
insert into pf_amigo values('simon','hugo');
insert into pf_amigo values('peter','hugo');
insert into pf_amigo values('hugo','peter');
insert into pf_amigo values('peter','simon');
insert into pf_amigo values('simon','peter');
insert into pf_amigo values('Jessie','Sarah');
insert into pf_amigo values('Sarah','Jessie');
insert into pf_amigo values('Sarah','Charles');
insert into pf_amigo values('Charles','Sarah');

-- Cinemas
insert into pf_cinema values('Aveiro');
insert into pf_cinema values('Porto');
insert into pf_cinema values('Lisboa');
insert into pf_cinema values('Coimbra');

-- Salas
declare @i int
set @i=2;
while (@i < 10)
begin
	insert into pf_sala values(@i,'Aveiro');
	insert into pf_sala values(@i,'Porto');
	insert into pf_sala values(@i,'Lisboa');
	insert into pf_sala values(@i,'Coimbra');
	set @i +=1
end

-- Membros da Cast
set @i=1
while(@i<=10)
begin
	insert into pf_membro_da_staff values('Realizador'+cast(@i as varchar),45,'Male',@i);
	insert into pf_realizador values(@i);
	insert into pf_membro_da_staff values('Actor'+cast((@i) as varchar),45,'Male',@i+10);
	insert into pf_actor values(@i+10);
	set @i +=1
end

-- Filmes
insert into pf_filme values('Rambo','A movie full of action.',2005,'02:30:00','ActionProducer',1);
insert into pf_filme values('RamboI','A movie full of action.',2006,'02:25:00','ActionProducer',1);
insert into pf_filme values('RamboII','A movie full of action.',2007,'02:20:00','ActionProducer',2);
insert into pf_filme values('RamboIII','A movie full of action.',2008,'02:20:00','ActionProducer',2);
insert into pf_filme values('RamboIV','A movie full of action',2009,'02:30:00','ActionProducer',2);
insert into pf_filme values('RamboV','A movie full of action. The last one.',2010,'02:45:00','ActionProducer',3);
insert into pf_filme values('A Risota','A movie full of comedy.',2012,'02:30:00','ComedyProducer',4);

-- Papeis dos Actores
insert into pf_actua values(11,'Rambo','ActionProducer','Personagem Principal');
insert into pf_actua values(12,'Rambo','ActionProducer','Heroi');
insert into pf_actua values(13,'Rambo','ActionProducer','Vilao');
insert into pf_actua values(14,'Rambo','ActionProducer','Vilao');
insert into pf_actua values(11,'RamboI','ActionProducer','Personagem Principal');
insert into pf_actua values(12,'RamboI','ActionProducer','Heroi');
insert into pf_actua values(13,'RamboI','ActionProducer','Vilao');
insert into pf_actua values(14,'RamboI','ActionProducer','Vilao');
insert into pf_actua values(11,'RamboII','ActionProducer','Personagem Principal');
insert into pf_actua values(12,'RamboII','ActionProducer','Heroi');
insert into pf_actua values(13,'RamboII','ActionProducer','Vilao');
insert into pf_actua values(14,'RamboII','ActionProducer','Vilao');
insert into pf_actua values(11,'RamboIII','ActionProducer','Personagem Principal');
insert into pf_actua values(12,'RamboIII','ActionProducer','Heroi');
insert into pf_actua values(13,'RamboIII','ActionProducer','Vilao');
insert into pf_actua values(14,'RamboIII','ActionProducer','Vilao');
insert into pf_actua values(11,'RamboIV','ActionProducer','Personagem Principal');
insert into pf_actua values(12,'RamboIV','ActionProducer','Heroi');
insert into pf_actua values(13,'RamboIV','ActionProducer','Vilao');
insert into pf_actua values(14,'RamboIV','ActionProducer','Vilao');
insert into pf_actua values(15,'A Risota','ComedyProducer','Apaixonado');
insert into pf_actua values(16,'A Risota','ComedyProducer','Personagem Principal');
insert into pf_actua values(17,'A Risota','ComedyProducer','Personagem Secundaria');

-- G�nero dos Filmes
insert into pf_genero_filme values('Rambo','ActionProducer','Accao');
insert into pf_genero_filme values('RamboI','ActionProducer','Accao');
insert into pf_genero_filme values('RamboII','ActionProducer','Accao');
insert into pf_genero_filme values('RamboIII','ActionProducer','Accao');
insert into pf_genero_filme values('RamboIV','ActionProducer','Accao');
insert into pf_genero_filme values('RamboV','ActionProducer','Accao');
insert into pf_genero_filme values('Rambo','ActionProducer','Terror');
insert into pf_genero_filme values('RamboI','ActionProducer','Terror');
insert into pf_genero_filme values('RamboII','ActionProducer','Terror');
insert into pf_genero_filme values('RamboIII','ActionProducer','Terror');
insert into pf_genero_filme values('RamboIV','ActionProducer','Terror');
insert into pf_genero_filme values('RamboV','ActionProducer','Terror');
insert into pf_genero_filme values('A Risota','ComedyProducer','Comedia');
insert into pf_genero_filme values('A Risota','ComedyProducer','Romance');

-- Recomenda��es
insert into pf_recomendacoes values('hugo','simon','Rambo','ActionProducer');
insert into pf_recomendacoes values('hugo','simon','RamboI','ActionProducer');
insert into pf_recomendacoes values('hugo','simon','RamboII','ActionProducer');
insert into pf_recomendacoes values('hugo','simon','RamboIII','ActionProducer');
insert into pf_recomendacoes values('hugo','simon','A Risota','ComedyProducer');

insert into pf_recomendacoes values('simon','hugo','Rambo','ActionProducer');
insert into pf_recomendacoes values('simon','hugo','RamboI','ActionProducer');
insert into pf_recomendacoes values('simon','hugo','RamboII','ActionProducer');
insert into pf_recomendacoes values('simon','hugo','RamboIII','ActionProducer');
insert into pf_recomendacoes values('simon','hugo','A Risota','ComedyProducer');

-- Sess�es
insert into pf_sessao values('14:00:00',convert(date,getdate()),1,'Rambo','ActionProducer','Aveiro',7);
insert into pf_sessao values('17:00:00',convert(date,getdate()),1,'Rambo','ActionProducer','Aveiro',7);
insert into pf_sessao values('20:00:00',convert(date,getdate()),1,'Rambo','ActionProducer','Aveiro',7);
insert into pf_sessao values('23:00:00',convert(date,getdate()),1,'Rambo','ActionProducer','Aveiro',7);
insert into pf_sessao values('14:00:00',convert(date,getdate()),2,'RamboI','ActionProducer','Aveiro',8);
insert into pf_sessao values('17:00:00',convert(date,getdate()),2,'RamboI','ActionProducer','Aveiro',8);
insert into pf_sessao values('20:00:00',convert(date,getdate()),2,'RamboI','ActionProducer','Aveiro',8);
insert into pf_sessao values('23:00:00',convert(date,getdate()),2,'RamboI','ActionProducer','Aveiro',8);

insert into pf_sessao values('14:00:00',convert(date,getdate()),1,'Rambo','ActionProducer','Coimbra',7);
insert into pf_sessao values('17:00:00',convert(date,getdate()),1,'Rambo','ActionProducer','Coimbra',7);
insert into pf_sessao values('20:00:00',convert(date,getdate()),1,'Rambo','ActionProducer','Coimbra',7);
insert into pf_sessao values('23:00:00',convert(date,getdate()),1,'Rambo','ActionProducer','Coimbra',7);
insert into pf_sessao values('14:00:00',convert(date,getdate()),2,'RamboI','ActionProducer','Coimbra',8);
insert into pf_sessao values('17:00:00',convert(date,getdate()),2,'RamboI','ActionProducer','Coimbra',8);
insert into pf_sessao values('20:00:00',convert(date,getdate()),2,'RamboI','ActionProducer','Coimbra',8);
insert into pf_sessao values('23:00:00',convert(date,getdate()),2,'RamboI','ActionProducer','Coimbra',8);

insert into pf_sessao values('14:00:00',convert(date,getdate()),1,'Rambo','ActionProducer','Porto',7);
insert into pf_sessao values('17:00:00',convert(date,getdate()),1,'Rambo','ActionProducer','Porto',7);
insert into pf_sessao values('20:00:00',convert(date,getdate()),1,'Rambo','ActionProducer','Porto',7);
insert into pf_sessao values('23:00:00',convert(date,getdate()),1,'Rambo','ActionProducer','Porto',7);
insert into pf_sessao values('14:00:00',convert(date,getdate()),2,'RamboI','ActionProducer','Porto',8);
insert into pf_sessao values('17:00:00',convert(date,getdate()),2,'RamboI','ActionProducer','Porto',8);
insert into pf_sessao values('20:00:00',convert(date,getdate()),2,'RamboI','ActionProducer','Porto',8);
insert into pf_sessao values('23:00:00',convert(date,getdate()),2,'RamboI','ActionProducer','Porto',8);

insert into pf_sessao values('14:00:00',convert(date,getdate()),1,'Rambo','ActionProducer','Lisboa',7);
insert into pf_sessao values('17:00:00',convert(date,getdate()),1,'Rambo','ActionProducer','Lisboa',7);
insert into pf_sessao values('20:00:00',convert(date,getdate()),1,'Rambo','ActionProducer','Lisboa',7);
insert into pf_sessao values('23:00:00',convert(date,getdate()),1,'Rambo','ActionProducer','Lisboa',7);
insert into pf_sessao values('14:00:00',convert(date,getdate()),2,'RamboI','ActionProducer','Lisboa',8);
insert into pf_sessao values('17:00:00',convert(date,getdate()),2,'RamboI','ActionProducer','Lisboa',8);
insert into pf_sessao values('20:00:00',convert(date,getdate()),2,'RamboI','ActionProducer','Lisboa',8);
insert into pf_sessao values('23:00:00',convert(date,getdate()),2,'RamboI','ActionProducer','Lisboa',8);

insert into pf_sessao values('14:00:00','2013-06-20',1,'A Risota','ComedyProducer','Aveiro',6);
insert into pf_sessao values('17:00:00','2013-06-20',1,'A Risota','ComedyProducer','Aveiro',6);
insert into pf_sessao values('20:00:00','2013-06-20',1,'A Risota','ComedyProducer','Aveiro',6);
insert into pf_sessao values('23:00:00','2013-06-20',1,'A Risota','ComedyProducer','Aveiro',6);
insert into pf_sessao values('14:00:00','2013-06-20',1,'A Risota','ComedyProducer','Coimbra',6);
insert into pf_sessao values('17:00:00','2013-06-20',1,'A Risota','ComedyProducer','Coimbra',6);
insert into pf_sessao values('20:00:00','2013-06-20',1,'A Risota','ComedyProducer','Coimbra',6);
insert into pf_sessao values('23:00:00','2013-06-20',1,'A Risota','ComedyProducer','Coimbra',6);

-- Reservas dos Membros
insert into pf_reserva values ('2013-05-25',1,'hugo');
insert into pf_reserva values ('2013-05-25',2,'simon');
insert into pf_reserva values ('2013-05-25',3,'peter');

insert into pf_bilhete values (6,'2013-06-20','14:00:00',1,'Aveiro','A',1,1);
insert into pf_bilhete values (6,'2013-06-20','14:00:00',1,'Aveiro','A',2,1);
insert into pf_bilhete values (6,'2013-06-20','14:00:00',1,'Aveiro','A',3,1);
insert into pf_bilhete values (6,'2013-06-20','14:00:00',1,'Aveiro','A',4,1);

insert into pf_bilhete values (6,'2013-06-20','17:00:00',1,'Aveiro','A',1,2);
insert into pf_bilhete values (6,'2013-06-20','17:00:00',1,'Aveiro','A',2,2);
insert into pf_bilhete values (6,'2013-06-20','17:00:00',1,'Aveiro','A',3,2);
insert into pf_bilhete values (6,'2013-06-20','17:00:00',1,'Aveiro','A',4,2);

insert into pf_bilhete values (6,'2013-06-20','20:00:00',1,'Aveiro','A',1,3);
insert into pf_bilhete values (6,'2013-06-20','20:00:00',1,'Aveiro','A',2,3);
insert into pf_bilhete values (6,'2013-06-20','20:00:00',1,'Aveiro','A',3,3);
insert into pf_bilhete values (6,'2013-06-20','20:00:00',1,'Aveiro','A',4,3);